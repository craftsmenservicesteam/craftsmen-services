﻿using Craftsmen.Database.Entity;
using Craftsmen.Database.Entity.images;
using Microsoft.EntityFrameworkCore;

namespace Craftsmen.Database.Context
{
    public class CraftsmenDbContext : DbContext
    {
        public CraftsmenDbContext(DbContextOptions<CraftsmenDbContext> options)
            : base(options)
        {

        }

        public DbSet<CraftOffer> CraftOffer { get; set; }
        public DbSet<CraftsmanProfile> CraftsmanProfile { get; set; }
        public DbSet<UserProfile> UserProfile { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Subcategory> Subcategory { get; set; }
        public DbSet<OfferImage> OfferImage { get; set; }
        public DbSet<Opinion> Opinion { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Subcategory>()
                .HasOne(p => p.Category)
                .WithMany(c => c.Subcategories)
                .HasForeignKey("CategoryId");

            modelBuilder.Entity<CraftOffer>()
                .HasOne(x => x.Category)
                .WithMany(x => x.CraftOffers)
                .HasForeignKey("SubcategoryId")
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasForeignKey("CategoryId");

            modelBuilder.Entity<CraftOffer>()
                .HasOne(t => t.Craftsman)
                .WithMany(x => x.CraftOffer)
                .HasForeignKey("CraftsmanId");

            modelBuilder.Entity<Opinion>()
                .HasOne(x => x.MarkedCraftsman)
                .WithMany(y => y.Opinions)
                .HasForeignKey(x => x.MarkedCraftsmanId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Opinion>()
                .HasOne(x => x.RaterUser)
                .WithMany(y => y.Opinions)
                .HasForeignKey(x => x.RaterUserId)
                .OnDelete(DeleteBehavior.SetNull);

            DbSeeder.Seed(modelBuilder);
        }
    }
}
