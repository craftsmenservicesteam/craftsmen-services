﻿using Craftsmen.Database.Entity;
using Craftsmen.Database.Entity.images;
using Microsoft.EntityFrameworkCore;
using System;

namespace Craftsmen.Database.Context
{
    public static class DbSeeder
    {
        static string defaultDesc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vestibulum est vel arcu aliquet pulvinar. Vivamus dapibus lobortis urna, id tincidunt ex feugiat vel. Vestibulum pellentesque, metus vitae ullamcorper fermentum, magna justo molestie lacus, id pharetra est magna id lorem. Sed ultrices magna eget magna varius scelerisque. Sed pretium arcu vel consequat semper. Aliquam erat volutpat. Aenean at arcu sit amet sapien mattis tincidunt eget et nunc. Vivamus ex magna, gravida a tempor ac, ultrices id tortor. Nulla pellentesque at libero vel blandit. Maecenas ut ante eros. Duis placerat, nisi vel posuere viverra, ipsum diam bibendum nunc, et luctus lectus mi ac diam. Donec varius porta nibh, id vulputate odio hendrerit sed. In convallis erat dolor, id viverra purus dapibus vitae. Vivamus eros tortor, vestibulum at rutrum rhoncus, tristique quis urna. Morbi interdum nunc id lacus cursus rhoncus. Ut at gravida purus.";

        public static void Seed(
            ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category() { Id = 1, Name = "Budowa domu" },
                new Category() { Id = 2, Name = "Remont" },
                new Category() { Id = 3, Name = "Projektowanie" },
                new Category() { Id = 4, Name = "Meble" },
                new Category() { Id = 5, Name = "Ogród" },
                new Category() { Id = 6, Name = "Malarz" },
                new Category() { Id = 7, Name = "Hydraulik" },
                new Category() { Id = 8, Name = "Elektryk" },
                new Category() { Id = 9, Name = "Montaż i naprawa" },
                new Category() { Id = 10, Name = "Złota rączka" },
                new Category() { Id = 11, Name = "Renowacja przedmiotów" },
                new Category() { Id = 12, Name = "Prace ręczne" }
            );

            modelBuilder.Entity<Subcategory>().HasData(
                new Subcategory() { Id = 1, Name = "Elewacja domu", CategoryId = 1 },
                new Subcategory() { Id = 2, Name = "Budowa dachu", CategoryId = 1 },
                new Subcategory() { Id = 3, Name = "Podłogi", CategoryId = 1 },
                new Subcategory() { Id = 4, Name = "Domy jednorodzinne", CategoryId = 1 },

                new Subcategory() { Id = 5, Name = "Kafelkowanie", CategoryId = 2 },
                new Subcategory() { Id = 6, Name = "Panele", CategoryId = 2 },
                new Subcategory() { Id = 7, Name = "Tynkowanie ścian", CategoryId = 2 },
                new Subcategory() { Id = 8, Name = "Naprawa domu", CategoryId = 2 },

                new Subcategory() { Id = 9, Name = "Architekt", CategoryId = 3 },
                new Subcategory() { Id = 10, Name = "Projektowanie wnętrz", CategoryId = 3 },
                new Subcategory() { Id = 11, Name = "Projektowanie mebli", CategoryId = 3 },
                new Subcategory() { Id = 12, Name = "Projektowanie ogrodu", CategoryId = 3 },

                new Subcategory() { Id = 13, Name = "Odnowa mebli", CategoryId = 4 },
                new Subcategory() { Id = 14, Name = "Składanie mebli", CategoryId = 4 },
                new Subcategory() { Id = 15, Name = "Meble na zamówienie", CategoryId = 4 },
                new Subcategory() { Id = 16, Name = "Recykling mebli", CategoryId = 4 },

                new Subcategory() { Id = 17, Name = "Wycinanie drzew", CategoryId = 5 },
                new Subcategory() { Id = 18, Name = "Pielęgnacja trawnika", CategoryId = 5 },
                new Subcategory() { Id = 19, Name = "Brukowanie", CategoryId = 5 },
                new Subcategory() { Id = 20, Name = "Projektowanie ogrodu", CategoryId = 5 },

                new Subcategory() { Id = 21, Name = "Malowanie ścian", CategoryId = 6 },
                new Subcategory() { Id = 22, Name = "Malowanie sufitów", CategoryId = 6 },
                new Subcategory() { Id = 23, Name = "Tapetowanie", CategoryId = 6 },
                new Subcategory() { Id = 24, Name = "Malowanie dachu", CategoryId = 6 },

                new Subcategory() { Id = 25, Name = "Instalacja wodna", CategoryId = 7 },
                new Subcategory() { Id = 26, Name = "Montaż kabiny prysznicowej", CategoryId = 7 },
                new Subcategory() { Id = 27, Name = "Pogotowie hydrauliczne", CategoryId = 7 },
                new Subcategory() { Id = 28, Name = "Biały montaż", CategoryId = 7 },

                new Subcategory() { Id = 29, Name = "Instalacje elektryczne", CategoryId = 8 },
                new Subcategory() { Id = 30, Name = "Pogotowie elektryczne", CategoryId = 8 },
                new Subcategory() { Id = 31, Name = "Pomiary elektryczne", CategoryId = 8 },
                new Subcategory() { Id = 32, Name = "Podłązcanie sprzętu AGD", CategoryId = 8 },

                new Subcategory() { Id = 33, Name = "Montaż okien", CategoryId = 9 },
                new Subcategory() { Id = 34, Name = "Montaż drzwi", CategoryId = 9 },
                new Subcategory() { Id = 35, Name = "Montaż klimatyzacji", CategoryId = 9 },
                new Subcategory() { Id = 36, Name = "Biały montaż", CategoryId = 9 },

                new Subcategory() { Id = 37, Name = "Regulacja drzwi i okien", CategoryId = 10 },
                new Subcategory() { Id = 38, Name = "Prace ogrodowe", CategoryId = 10 },
                new Subcategory() { Id = 39, Name = "Naprawa sprzętu AGD", CategoryId = 10 },
                new Subcategory() { Id = 40, Name = "Drobne naprawy", CategoryId = 10 },

                new Subcategory() { Id = 41, Name = "Renowacja butów", CategoryId = 11 },
                new Subcategory() { Id = 42, Name = "Renowacja pojazdów zabytkowych", CategoryId = 11 },
                new Subcategory() { Id = 43, Name = "Renowacja drobnych urządzeń", CategoryId = 11 },
                new Subcategory() { Id = 44, Name = "Odnawianie starych zdjęć", CategoryId = 11 },

                new Subcategory() { Id = 45, Name = "Krawiec", CategoryId = 12 },
                new Subcategory() { Id = 46, Name = "Szewc", CategoryId = 12 },
                new Subcategory() { Id = 47, Name = "Zegarmistrz", CategoryId = 12 },
                new Subcategory() { Id = 48, Name = "Kowal", CategoryId = 12 }
            );

            modelBuilder.Entity<UserProfile>().HasData(
                new UserProfile() { Id = 1, Name = "Adam", Surname = "Strzelecki", Email = "adam.strzelecki@gmail.com", IsCraftsman = true, AuthId = "auth0|5da8dac469922c0e9008a239" },
                new UserProfile() { Id = 2, Name = "Krzysztof", Surname = "Szewczyk", Email = "krzysztof.szewczyk@gmail.com", IsCraftsman = true, AuthId = "auth0|5da8dac469922c0e9008a238" },
                new UserProfile() { Id = 3, Name = "Małgorzata", Surname = "Sulek", Email = "malgorzata.sulek@gmail.com", IsCraftsman = true, AuthId = "auth0|5da8dac469922c0e9008a237" },
                new UserProfile() { Id = 4, Name = "Zygmunt", Surname = "Zawisza", Email = "zygmunt.zawisza@gmail.com", IsCraftsman = true, AuthId = "auth0|5da8dac469922c0e9008a236" },
                new UserProfile() { Id = 5, Name = "Grzegorz", Surname = "Frątczak", Email = "grzegorz.fratczak@gmail.com", IsCraftsman = true, AuthId = "auth0|5da8dac469922c0e9008a235" },
                new UserProfile() { Id = 6, Name = "Wojciech", Surname = "Sterman", Email = "wojciech.sterman@gmail.com", IsCraftsman = true, AuthId = "auth0|5da8dac469922c0e9008a234" },
                new UserProfile() { Id = 7, Name = "Anna", Surname = "Manko", Email = "anna.manko@gmail.com", IsCraftsman = true, AuthId = "auth0|5da8dac469922c0e9008a233" }
            );

            modelBuilder.Entity<CraftsmanProfile>().HasData(
                new {
                    Id = (long)1,
                    UserProfileId = (long)1,
                    Company = "StrzeleckiBud",
                    Description = defaultDesc,
                    PhoneNumber = "500 145 587",
                    Latitude = 54.3716395,
                    Longitude = 18.6123305,
                    Monday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Tuesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Wednesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Thursday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Friday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Saturday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Sunday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Business = "Budowa domu",
                    Rate = 4.7
                },
                new
                {
                    Id = (long)2,
                    UserProfileId = (long)2,
                    Company = "Szewczyk Dratewka",
                    Description = defaultDesc,
                    PhoneNumber = "580 240 268",
                    Latitude = 53.4296143,
                    Longitude = 14.4845391,
                    Monday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Tuesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Wednesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Thursday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Friday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Saturday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Sunday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Business = "Szewc",
                    Rate = 4.9
                },
                new
                {
                    Id = (long)3,
                    UserProfileId = (long)3,
                    Company = "Twoja Krawcowa",
                    Description = defaultDesc,
                    PhoneNumber = "608 978 541",
                    Latitude = 54.207139,
                    Longitude = 16.076918,
                    Monday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Tuesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Wednesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Thursday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Friday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Saturday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Sunday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Business = "Krawiectwo",
                    Rate = 4.7
                } ,
                new
                {
                    Id = (long)4,
                    UserProfileId = (long)4,
                    Company = "Zawsze na czas",
                    Description = defaultDesc,
                    PhoneNumber = "854 231 444",
                    Latitude = 51.7730347,
                    Longitude = 19.3405077,
                    Monday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Tuesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Wednesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Thursday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Friday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Saturday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Sunday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Business = "Krawiectwo",
                    Rate = 4.3
                } ,
                new
                {
                    Id = (long)5,
                    UserProfileId = (long)5,
                    Company = "Kowal Grzegorz",
                    Description = defaultDesc,
                    PhoneNumber = "852 211 313",
                    Latitude = 53.013372,
                    Longitude = 18.5315086,
                    Monday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Tuesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Wednesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Thursday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Friday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Saturday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Sunday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Business = "Krawiectwo",
                    Rate = 4.9
                },
                new
                {
                    Id = (long)6,
                    UserProfileId = (long)6,
                    Company = "Old Car Studio",
                    Description = defaultDesc,
                    PhoneNumber = "784 544 644",
                    Latitude = 51.1269942,
                    Longitude = 16.8517789,
                    Monday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Tuesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Wednesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Thursday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Friday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Saturday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Sunday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Business = "Krawiectwo",
                    Rate = 5.0
                },
                new
                {
                    Id = (long)7,
                    UserProfileId = (long)7,
                    Company = "Studio CYK!",
                    Description = defaultDesc,
                    PhoneNumber = "645 852 333",
                    Latitude = 52.2326063,
                    Longitude = 20.781007,
                    Monday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Tuesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Wednesday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Thursday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Friday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Saturday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Sunday = "{\"StartTime\":\"08:30\",\"EndTime\":\"16:30\"}",
                    Business = "Krawiectwo",
                    Rate = 4.2
                }
            );


            modelBuilder.Entity<CraftOffer>().HasData(
                new { Id = (long)1, Title = "Budowa domu jednorodzinnego", CategoryId = (long)1, SubcategoryId = (long)4, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Trójmiasto i okolice", Price = (float)50000, PricePerHour = (float)100, DoorToDoor = true, CraftsmanId = (long)1, ImageId = (long)5 },
                new { Id = (long)2, Title = "Kładzenie paneli podłogowych", CategoryId = (long)1, SubcategoryId = (long)3, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Trójmiasto i okolice", Price = (float)6000, PricePerHour = (float)100, DoorToDoor = true, CraftsmanId = (long)1, ImageId = (long)6 },
                new { Id = (long)3, Title = "Budowa dachów, kładzenie dachówki", CategoryId = (long)1, SubcategoryId = (long)2, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Trójmiasto i okolice", Price = (float)8000, PricePerHour = (float)90, DoorToDoor = true, CraftsmanId = (long)1, ImageId = (long)4 },
                new { Id = (long)4, Title = "Wykończenia zewnętrzne domu", CategoryId = (long)1, SubcategoryId = (long)1, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Trójmiasto i okolice", Price = (float)7000, PricePerHour = (float)150, DoorToDoor = true, CraftsmanId = (long)1, ImageId = (long)3 },

                new { Id = (long)5, Title = "Sukienki na miarę", CategoryId = (long)12, SubcategoryId = (long)45, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Szczecin", Price = (float)150, PricePerHour = (float)25, DoorToDoor = false, CraftsmanId = (long)3, ImageId = (long)1 },
                new { Id = (long)6, Title = "Garnitury na wszelkie okazje", CategoryId = (long)12, SubcategoryId = (long)45, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Szczecin", Price = (float)400, PricePerHour = (float)35, DoorToDoor = false, CraftsmanId = (long)3, ImageId = (long)2 },
                new { Id = (long)7, Title = "Naprawa obuwia", CategoryId = (long)12, SubcategoryId = (long)46, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Koszalin", Price = (float)80, PricePerHour = (float)20, DoorToDoor = false, CraftsmanId = (long)2, ImageId = (long)7 },
                new { Id = (long)8, Title = "Malowanie butów", CategoryId = (long)12, SubcategoryId = (long)46, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Koszalin", Price = (float)150, PricePerHour = (float)30, DoorToDoor = false, CraftsmanId = (long)2, ImageId = (long)8 },

                new { Id = (long)9, Title = "Naparawa zegarków", CategoryId = (long)12, SubcategoryId = (long)47, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Łódź i okolice", Price = (float)180, PricePerHour = (float)60, DoorToDoor = false, CraftsmanId = (long)4, ImageId = (long)9 },
                new { Id = (long)10, Title = "Renowacja starych zegarów", CategoryId = (long)12, SubcategoryId = (long)47, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Łódź i okolice", Price = (float)250, PricePerHour = (float)50, DoorToDoor = false, CraftsmanId = (long)4, ImageId = (long)10 },
                new { Id = (long)11, Title = "Kucie bram, balustrad i ogrodzeń na zamówienie", CategoryId = (long)12, SubcategoryId = (long)48, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Toruń", Price = (float)6000, PricePerHour = (float)80, DoorToDoor = true, CraftsmanId = (long)5, ImageId = (long)11 },
                new { Id = (long)12, Title = "Renowacja bram i balustrad", CategoryId = (long)12, SubcategoryId = (long)48, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Toruń", Price = (float)5000, PricePerHour = (float)90, DoorToDoor = true, CraftsmanId = (long)5, ImageId = (long)12 },

                new { Id = (long)13, Title = "Renowacja zabytkowych samochodów", CategoryId = (long)11, SubcategoryId = (long)42, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Wrocław", Price = (float)20000, PricePerHour = (float)120, DoorToDoor = false, CraftsmanId = (long)6, ImageId = (long)13 },
                new { Id = (long)14, Title = "Wytwarzanie części do zabytkowych samochodów", CategoryId = (long)11, SubcategoryId = (long)42, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Wrocław", Price = (float)10000, PricePerHour = (float)150, DoorToDoor = false, CraftsmanId = (long)6, ImageId = (long)14 },
                new { Id = (long)15, Title = "Odnowa starych zdjęć", CategoryId = (long)11, SubcategoryId = (long)44, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Warszawa", Price = (float)300, PricePerHour = (float)80, DoorToDoor = false, CraftsmanId = (long)7, ImageId = (long)15 },
                new { Id = (long)16, Title = "Cyfryzacja starych zdjęć", CategoryId = (long)11, SubcategoryId = (long)44, HasOwnTools = true, Description = defaultDesc, CreationDate = new System.DateTime(), AvailableLocation = "Warszawa", Price = (float)120, PricePerHour = (float)40, DoorToDoor = false, CraftsmanId = (long)7, ImageId = (long)16 }
            );


            modelBuilder.Entity<OfferImage>().HasData(
                new { Id = (long)1, OfferId = (long)5, Image = TestImage.ImageCloths },
                new { Id = (long)2, OfferId = (long)6, Image = TestImage.ImageSuit },
                new { Id = (long)3, OfferId = (long)4, Image = TestImage.ImageElevation },
                new { Id = (long)4, OfferId = (long)3, Image = TestImage.ImageRoof },
                new { Id = (long)5, OfferId = (long)1, Image = TestImage.ImageHouseBuilding },
                new { Id = (long)6, OfferId = (long)2, Image = TestImage.ImageFloorPanel },
                new { Id = (long)7, OfferId = (long)7, Image = TestImage.ImageShoeRepair },
                new { Id = (long)8, OfferId = (long)8, Image = TestImage.ImageShoePainting },
                new { Id = (long)9, OfferId = (long)9, Image = TestImage.ImageWatch },
                new { Id = (long)10, OfferId = (long)10, Image = TestImage.ImageOldWatches },
                new { Id = (long)11, OfferId = (long)11, Image = TestImage.ImageFence },
                new { Id = (long)12, OfferId = (long)12, Image = TestImage.ImageBalustrade },
                new { Id = (long)13, OfferId = (long)13, Image = TestImage.ImageOldCar },
                new { Id = (long)14, OfferId = (long)14, Image = TestImage.ImageOldCarPart },
                new { Id = (long)15, OfferId = (long)15, Image = TestImage.ImageOldPhoto },
                new { Id = (long)16, OfferId = (long)16, Image = TestImage.ImageOldPhotoDigital }
            );

            modelBuilder.Entity<Opinion>().HasData(
                new { Id = (long)1, Rate = 3, Description = "Bardzo średni rzemieślnik. Wykonał prace ze sporym opóźnieniem", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)1, RaterUserId = (long)2 },
                new { Id = (long)2, Rate = 5, Description = "Budowa bardzo dobrze wykonana, jestem niezmiernie zadowolony z efektu. Polecam tego rzemieślnika!", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)1, RaterUserId = (long)3 },
                new { Id = (long)3, Rate = 3, Description = "Słaby kontakt i lekkie opóźnienie, ale praca została wykonana należycie", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)1, RaterUserId = (long)4 },
                new { Id = (long)4, Rate = 4, Description = "Kompetentny wykonawca, zdecydowanie zna się na swoim fachu. Gorąco polecam!", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)1, RaterUserId = (long)5 },
                new { Id = (long)5, Rate = 5, Description = "Moje buty wyglądają jak nowe! Usługa warta każdej złotówki! Polecam!", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)2, RaterUserId = (long)1 },
                new { Id = (long)6, Rate = 4, Description = "Lata doświadczenia przekładają się na fascynujący efekt! Nie sądziłam, że moje buty jeszcze kiedyś będą wyglądać jak nowe!", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)2, RaterUserId = (long)3 },
                new { Id = (long)7, Rate = 5, Description = "Rewelacyjna krawcowa! Słuchała wszystkich sugestii i sama zaproponowała kilka poprawek, a końcowy efekt przerósł moje oczekiwania!", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)3, RaterUserId = (long)3 },
                new { Id = (long)8, Rate = 4, Description = "Bardzo dobra krawcowa, zdecydowanie polecam jeśli ktoś się jeszcze waha! Uwaga na długie kolejki jest rozchwytywana! :)", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)3, RaterUserId = (long)7 },
                new { Id = (long)9, Rate = 4, Description = "Szybka i sprawna wymiana baterii i paska w zegarku.", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)4, RaterUserId = (long)6 },
                new { Id = (long)10, Rate = 5, Description = "Balustrady kute na zamówienie, według mojego pomysłu, wzbogacone o sugestie kowala. Choć cena nie była mała, efekt jest tego wart", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)5, RaterUserId = (long)4 },
                new { Id = (long)11, Rate = 4, Description = "Ten kowal zna się na rzeczy, wykuł bramę z wzorami kwiatowymi według mojego widzimisię, co nie jest zwykle proste do sprostania!", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)5, RaterUserId = (long)2 },
                new { Id = (long)12, Rate = 4, Description = "Moja stara nyska wygląda teraz przepięknie!", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)6, RaterUserId = (long)1 },
                new { Id = (long)13, Rate = 3, Description = "Oddałem do renowacji auto mojego taty, oczekiwałem iż zostaną wymienione wszystkie skorodowane części co jednak nie miało miejsca", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)6, RaterUserId = (long)2 },
                new { Id = (long)14, Rate = 2, Description = "Wyszłam okropnie grubo na zdjęciach, należy mi się zwrot pieniędzy!", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)7, RaterUserId = (long)3 },
                new { Id = (long)15, Rate = 5, Description = "Zdjęcia mojej prapraprababci zostały odtworzone tak, że wyglądają jakby były zrobione wczoraj!", CreationDate = DateTime.Now, MarkedCraftsmanId = (long)7, RaterUserId = (long)4 }
            );

        }
    }
}
