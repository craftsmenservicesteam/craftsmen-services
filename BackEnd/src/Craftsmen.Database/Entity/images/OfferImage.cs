﻿using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Database.Entity.images
{
    public class OfferImage
    {
        [Key]
        public long Id { get; set; }
        public CraftOffer Offer { get; set; }
        [StringLength(int.MaxValue)]
        public string Image { get; set; }
    }
}
