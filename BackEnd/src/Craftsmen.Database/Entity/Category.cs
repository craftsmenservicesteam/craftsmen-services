﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Database.Entity
{
    public class Category
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }

        public ICollection<CraftOffer> CraftOffers { get; set; } = new List<CraftOffer>();
        public ICollection<Subcategory> Subcategories { get; set; } = new HashSet<Subcategory>();
    }
}
