﻿using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Database.Entity
{
    public class Subcategory
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public long CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
