﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Database.Entity
{
    public class UserProfile
    {
        [Key]
        public long Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string AuthId { get; set;
        }
        [Required(AllowEmptyStrings = false)]
        public string Surname { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }

        [Required]
        public bool IsCraftsman { get; set; }

        public IEnumerable<Opinion> Opinions { get; set; }
            = new List<Opinion>();
    }
}
