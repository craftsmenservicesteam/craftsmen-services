﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Database.Entity
{
    public class CraftsmanProfile
    {
        [Key]
        public long Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Company { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Description { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string PhoneNumber { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Business { get; set; }
        public double? Rate { get; set; }
        public long UserProfileId { get; set; }

        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
        public UserProfile UserProfile { get; set; }
        public IEnumerable<CraftOffer> CraftOffer { get; set; }
            = new List<CraftOffer>();
        public IEnumerable<Opinion> Opinions { get; set; }
            = new List<Opinion>();
    }
}
