﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Database.Entity
{
    public class Opinion
    {
        [Key]
        public long Id { get; set; }

        public int Rate { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }

        public CraftsmanProfile MarkedCraftsman { get; set; }
        public long MarkedCraftsmanId { get; set; }

        public UserProfile RaterUser { get; set; }
        public long? RaterUserId { get; set; }
    }
}
