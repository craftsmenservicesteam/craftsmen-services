﻿using Craftsmen.Database.Entity.images;
using System;
using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Database.Entity
{
    public class CraftOffer 
    {
        [Key]
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public Category Category { get; set; }
        public Subcategory Subcategory { get; set; }
        public string AvailableLocation { get; set; }
        public float Price { get; set; }
        public float PricePerHour { get; set; }
        public bool DoorToDoor { get; set; }
        public bool HasOwnTools { get; set; }
        public CraftsmanProfile Craftsman { get; set; }
        public long CraftsmanId { get; set; }
        public OfferImage Image { get; set; }
        public long ImageId { get; set; }
        public long CategoryId;
        public long SubcategoryId;
    }
}
