﻿using Craftsmen.Contract.DTO.Categories;
using Craftsmen.Contract.DTO.Offer;
using Craftsmen.Core.Model;
using Craftsmen.Core.Services.Offer;
using Craftsmen.Core.Utils;
using Craftsmen.Core.Utils.Result;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Craftsmen.WebApi.Controllers
{
    [Route("Public/[controller]")]
    [ApiController]
    public class CraftOfferController : ControllerBase
    {
        private readonly IAddCraftOfferService addOfferService;
        private readonly IGetCraftOfferService getOfferService;
        private readonly IGetCategoriesService getCategoriesService;
        private readonly IGetSubcategoriesService getSubcategoriesService;

        public CraftOfferController(
            IAddCraftOfferService addOfferService,
            IGetCraftOfferService getOfferService,
            IGetCategoriesService getCategoriesService,
            IGetSubcategoriesService getSubcategoriesService)
        {
            this.addOfferService = addOfferService;
            this.getOfferService = getOfferService;
            this.getCategoriesService = getCategoriesService;
            this.getSubcategoriesService = getSubcategoriesService;
        }

        [HttpPost("Add")]
        [Authorize]
        public async Task<ActionResult<bool>> Add([FromBody]AddCraftOfferDTO offer)
        {
            var result = await addOfferService.Add(offer, IdentityHelper.Id(User));
            if (result)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest("Error");
            }
        }

        [HttpPost("SearchMany")]
        public async Task<ActionResult<GetManyCraftOffersDTO>> GetMany([FromBody] SearchManyFiltersDTO dto)
        {
            return await getOfferService.GetMany(dto.offset, dto.hits);
        }

        [HttpGet("GetCategories")]
        public async Task<ActionResult<GetCategoriesDTO[]>> GetCategories()
        {
            return await getCategoriesService.GetAllAsync();
        }

        [HttpGet("GetSubcategories")]
        public async Task<ActionResult<GetSubcategoriesDTO[]>> GetSubcategories()
        {
            return await getSubcategoriesService.GetAllAsync();
        }
        
        [HttpGet("GetOfferById/{id}")]
        public async Task<Result<GetOneCraftOfferDTO>> GetOfferById(int id)
        {
            return ResultBuilder<GetOneCraftOfferDTO>.Succeded(
                await getOfferService.GetOne(id));
        }

        [HttpGet("GetManyWithFilters/{offset}/{hits}")]
        public async Task<Result> GetManyWithFiltersAsync(
            int offset,
            int hits,
            [FromQuery(Name = "category")] int categoryId,
            [FromQuery(Name = "contains")] string contains,
            [FromQuery(Name = "location")] string location)
        {

            var result = await getOfferService.GetManyWithFiltersAsync(new Filters()
            {
                Offset = offset,
                Hits = hits,
                CategoryId = categoryId,
                Contains = contains ?? "",
                Location = location ?? ""
            });

            return ResultBuilder<GetManyCraftOffersDTO>.Succeded(result);
        }

        [HttpGet("GetOffersByCraftsmanId/{id}")]
        public async Task<Result> GetCraftsmanOffersById(int id)
        {
            var result = await getOfferService.GetOffersByCraftsmanId(id);
            return ResultBuilder<GetManyCraftOffersDTO>
                .Succeded(result);
        }
    }
}
