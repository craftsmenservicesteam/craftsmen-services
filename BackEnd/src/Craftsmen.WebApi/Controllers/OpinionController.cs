﻿using Craftsmen.Contract.DTO.Opinion;
using Craftsmen.Core.Services.Opinions;
using Craftsmen.Core.Utils;
using Craftsmen.Core.Utils.Result;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Craftsmen.WebApi.Controllers
{
    [Route("Public/[controller]")]
    [ApiController]
    public class OpinionController: ControllerBase
    {
        private readonly IOpinionService _service;

        public OpinionController(IOpinionService service)
        {
            _service = service;
        }

        [HttpGet("GetMany/{id}")]
        public async Task<Result> GetManyByCraftsmanId(long id)
        {
            return ResultBuilder<IEnumerable<OpinionDTO>>.Succeded(
                await _service.GetManyByCraftsmanId(id));
        }

        [HttpPost("Add")]
        [Authorize]
        public async Task<Result> Add([FromBody] AddOpinionDTO dto)
        {
            return await _service.Add(dto, IdentityHelper.Id(User))
                ? ResultBuilder.Succeded()
                : ResultBuilder.Failed("Failed to add opinion");
        }
    }
}
