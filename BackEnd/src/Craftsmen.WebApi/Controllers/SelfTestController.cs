﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Craftsmen.WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SelfTestController: ControllerBase
    {
        [HttpGet("IsAlive")]
        public string IsAlive()
        {
            return "ALIVE";
        }

        [HttpGet("Private")]
        [Authorize]
        public string IsAuthenticated()
        {
            return "AUTHENTICATED";
        }
    }
}
