﻿using Craftsmen.Contract.DTO.UserProfile;
using Craftsmen.Core.Services.UserProfileService;
using Craftsmen.Core.Utils.Result;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Craftsmen.Core.Utils;

namespace Craftsmen.WebApi.Controllers
{
    [Route("Public/[controller]")]
    public class UserProfileController: ControllerBase
    {
        private readonly IUserProfileService _service;
        private readonly IConfiguration _configuration;

        public UserProfileController(IUserProfileService service, IConfiguration configuration)
        {
            _service = service;
            _configuration = configuration;
        }

        [HttpGet("GetUserProfileById/{id}")]
        public async Task<Result> GetUserProfileById(long id)
        {
            var dto = await _service.GetUserProfileById(id);
            return dto != null
                ? ResultBuilder<CraftsmanProfileDTO>.Succeded(dto)
                : ResultBuilder.Failed("Failed to get a user");
        }

        [HttpPost("AddMyProfile")]
        [Authorize]
        public async Task<Result> AddMyProfile([FromBody] CraftsmanProfileDTO userProfile)
        {
            return await _service.AddMyProfile(userProfile, IdentityHelper.Id(User))
                ? ResultBuilder.Succeded()
                : ResultBuilder.Failed("Failded to add user profile");
        }

        [HttpGet("GetMyProfile")]
        public async Task<Result> GetMyProfile()
        {
            var dto = await _service.GetMyProfile(IdentityHelper.Id(User));
            return dto != null
                ? ResultBuilder<CraftsmanProfileDTO>.Succeded(dto)
                : ResultBuilder.Failed("Failed to get my profile");
        }

        [HttpPut("UpdateMyProfile")]
        [Authorize]
        public async Task<Result> UpdateMyProfile([FromBody] CraftsmanProfileDTO userProfile)
        {
            return await _service.UpdateMyProfile(userProfile, IdentityHelper.Id(User))
                ? ResultBuilder.Succeded()
                : ResultBuilder.Failed("Failed to update user profile");
        }
    }
}
