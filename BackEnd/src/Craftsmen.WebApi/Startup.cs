﻿// Connection With Auth0 API source: https://auth0.com/docs/quickstart/backend/aspnet-core-webapi/01-authorization
// Taken from: https://github.com/andaro74/Angular-5-Net-Core-Web-API-with-Auth0
using AutoMapper;
using Craftsmen.Database.Context;
using Craftsmen.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using StructureMap;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Craftsmen.WebApi.Handlers;

namespace Craftsmen.WebApi
{
    public class Startup : IStartup
    {
        private readonly IHostingEnvironment _environment;
        public IConfiguration _configuration { get; }
        private readonly ServicesRegistry _servicesRegistry;

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            _environment = environment;
            _configuration = configuration;
            _servicesRegistry = new ServicesRegistry(_configuration);
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsDevPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials();
                });

            });

            services.AddMvc(options => 
            {
                options.Filters.Add(typeof(ValidateModelStateAttribute));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            AddAuth0(services);
            var connectionString = @"Server=mssql;Database=Craftsmen;User=sa;Password=Your_password123;";

            services.AddDbContext<CraftsmenDbContext>(options =>
                // options.UseSqlServer("Server=localhost;Database=Craftsmen;Trusted_Connection=True"));
                options.UseSqlServer(connectionString));

            var container = new Container();

            container.Configure(config =>
            {
                config.AddRegistry(_servicesRegistry);
                config.Populate(services);
            });

            return container.GetInstance<IServiceProvider>();
        }

        private void AddAuth0(IServiceCollection services)
        {
            string domain = $"https://kamil-mastalerz.eu.auth0.com/";
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;


            }).AddJwtBearer(options =>
            {
                options.Authority = domain;
                options.Audience = "https://craftsmen-api/";

                options.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        // Grab the raw value of the token, and store it as a claim so we can retrieve it again later in the request pipeline
                        if (context.SecurityToken is JwtSecurityToken token)
                        {
                            if (context.Principal.Identity is ClaimsIdentity identity)
                            {
                                identity.AddClaim(new Claim("accessToken", token.RawData));
                            }
                        }

                        return Task.FromResult(0);
                    }
                };
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("read:messages", policy => policy.Requirements.Add(new HasScopeRequirement("read:messages", domain)));

            });

            // register the scope authorization handler
            services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();
        }

        public void Configure(IApplicationBuilder app)
        {
            if (_environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors("CorsDevPolicy");
            }

            Mapper.Initialize(config =>
            {
                new MapCreator(config).Create();
            });
            app.UseHttpsRedirection();
            
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
