﻿using AutoMapper;
using Craftsmen.Contract.DTO.Offer;
using Craftsmen.Contract.DTO.UserProfile;
using Craftsmen.Contract.DTO.Categories;
using Craftsmen.Database.Entity;
using Craftsmen.Infrastructure.JsonConverters;
using System;
using Craftsmen.Contract.DTO.Opinion;

namespace Craftsmen.Infrastructure
{
    public class MapCreator
    {
        private readonly IMapperConfigurationExpression _config;

        public MapCreator(IMapperConfigurationExpression config)
        {
            _config = config;
        }

        public void Create()
        {
            _config.CreateMap<AddCraftOfferDTO, CraftOffer>()
                .AfterMap((x, y) =>
                {
                    y.CreationDate = DateTime.Now;
                });

            _config.CreateMap<Category, GetCategoriesDTO>();
            _config.CreateMap<Subcategory, GetSubcategoriesDTO>();
            _config.CreateMap<CraftOffer, GetCraftOffersDTO>();

            _config.CreateMap<CraftOffer, GetCraftOffersDTO>();

            _config.CreateMap<CraftOffer, GetOneCraftOfferDTO>();

            _config.CreateMap<CraftsmanProfileDTO, CraftsmanProfile>();
            _config.CreateMap<CraftsmanProfileDTO, UserProfile>();

            _config.CreateMap<UserProfile, CraftsmanProfileDTO>();
            _config.CreateMap<CraftsmanProfile, CraftsmanProfileDTO>()
                .ConvertUsing(new CraftsmanProfileConverter());

            _config.CreateMap<OpinionDTO, Opinion>();
            _config.CreateMap<Opinion, OpinionDTO>();
            _config.CreateMap<AddOpinionDTO, Opinion>();
        }
    }
}
