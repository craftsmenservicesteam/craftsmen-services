﻿using AutoMapper;
using Craftsmen.Contract.DTO.Shared;
using Craftsmen.Contract.DTO.UserProfile;
using Craftsmen.Database.Entity;

namespace Craftsmen.Infrastructure.JsonConverters
{
    public class CraftsmanProfileConverter:
        ITypeConverter<CraftsmanProfile, CraftsmanProfileDTO>
    {
        public CraftsmanProfileDTO Convert(
            CraftsmanProfile source,
            CraftsmanProfileDTO destination,
            ResolutionContext context)
        {
            destination = new CraftsmanProfileDTO();
            JsonConversionResolver<TimeRangeDTO>.Resolve(source, destination);
            destination.CraftsmanId = source.Id;
            return destination;
        }
    }
}
