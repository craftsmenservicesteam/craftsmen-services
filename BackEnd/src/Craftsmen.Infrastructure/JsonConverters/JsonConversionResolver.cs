﻿using Newtonsoft.Json;

namespace Craftsmen.Infrastructure.JsonConverters
{
    public static class JsonConversionResolver<T>
    {
        public static void Resolve(object from, object to)
        {
            foreach (var property in from.GetType().GetProperties())
            {
                var toProperty = to.GetType().GetProperty(property.Name);

                if (toProperty != null)
                {
                    var toType = toProperty.PropertyType;
                    var fromType = property.PropertyType;
                    var fromValue = property.GetValue(from);

                    if (toType == fromType)
                    {
                        toProperty.SetValue(to, fromValue);
                    }
                    else if (fromType == typeof(string) && fromValue != null)
                    {
                        toProperty.SetValue(to, JsonConvert.DeserializeObject<T>((string)fromValue));
                    }
                }
            }
        }
    }
}
