﻿using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Infrastructure
{
    public class EnvironmentConfig
    {
        public static string ConnectionString { get; set; }
        public const string ConfigurationPrefix = "CRAFTSMEN";

        public static EnvironmentConfig GetInstance(IConfiguration configurationRoot)
        {
            var config = new EnvironmentConfig();
            configurationRoot.Bind(ConfigurationPrefix, config);
            Validator.ValidateObject(config, new ValidationContext(config), true);
            return config;
        }
    }
}
