﻿using Craftsmen.Core.Repository;
using Craftsmen.Core.Services.Offer;
using Craftsmen.Core.Services.Opinions;
using Craftsmen.Core.Services.UserProfileService;
using Microsoft.Extensions.Configuration;
using StructureMap;

namespace Craftsmen.Infrastructure
{
    public class ServicesRegistry: Registry
    {
        public ServicesRegistry(IConfiguration configuration)
        {
            // Services
            For<EnvironmentConfig>().Singleton().Use(EnvironmentConfig.GetInstance(configuration));
            For<IAddCraftOfferService>().Use<AddCraftOfferService>();
            For<IGetCraftOfferService>().Use<GetCraftOfferService>();
            For<IUserProfileService>().Use<UserProfileService>();
            For<IGetCategoriesService>().Use<GetCategoriesService>();
            For<IGetSubcategoriesService>().Use<GetSubcategoryService>();
            For<IOpinionService>().Use<OpinionService>();

            // Database
            For<ICraftOfferRepository>().Use<CraftOfferRepository>();
            For<IUserProfileRepository>().Use<UserProfileRepository>();
            For<ICategoryRepository>().Use<CategoryRepository>();
            For<ISubcategoryRepository>().Use<SubcategoryRepository>();
            For<IImagesRepository>().Use<ImagesRepository>();
            For<IOpinionRepository>().Use<OpinionRepository>();
        }
    }
}
