﻿namespace Craftsmen.Core.Model
{
    public class Filters
    {
        public int Offset { get; set; }
        public int Hits { get; set; }
        public int CategoryId { get; set; }
        public string Contains { get; set; }
        public string Location { get; set; }
    }
}
