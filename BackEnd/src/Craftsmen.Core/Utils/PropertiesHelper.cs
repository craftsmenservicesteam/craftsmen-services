﻿using System;

namespace Craftsmen.Core.Utils
{
    public static class PropertiesHelper
    {
        public static void CopyProperties(
            Object target,
            Object source)
        {
            if (target == null || source == null)
                return;

            foreach (var property in source.GetType().GetProperties())
            {
                if (property.GetValue(source) != null
                    && target.GetType().GetProperty(property.Name) != null)
                {
                    property.SetValue(target, property.GetValue(source));
                }
            }
        }
    }
}
