﻿using System.Security.Claims;
using System.Linq;

namespace Craftsmen.Core.Utils
{
    public static class IdentityHelper
    {
        public static string Id(ClaimsPrincipal User) => User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
    }
}
