﻿namespace Craftsmen.Core.Utils.Result
{
    public class Result
    {
        public bool Succeeded { get; set; }
        public string Message { get; set; }
    }

    public class Result<T> : Result
    {
        public new bool Succeeded { get; set; }
        public new bool Message { get; set; }
        public Result(T body)
        {
            Body = body;
        }
        public T Body { get; set; }
    }
}
