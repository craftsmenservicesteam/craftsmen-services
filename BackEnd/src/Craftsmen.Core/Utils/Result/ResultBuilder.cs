﻿namespace Craftsmen.Core.Utils.Result
{
    public static class ResultBuilder
    {
        public static Result Succeded() => new Result() { Succeeded = true };
        public static Result Failed(string message) => new Result() { Succeeded = false, Message = message };
    }

    public static class ResultBuilder<T>
    {
        public static Result<T> Succeded(T body) => new Result<T>(body) { Succeeded = true };
    }
}
