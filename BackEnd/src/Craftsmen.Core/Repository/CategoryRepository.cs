﻿using Craftsmen.Database.Context;
using Craftsmen.Database.Entity;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Craftsmen.Core.Repository
{
    public interface ICategoryRepository
    {
        Task <long>Add(Category category);
        Task<Category> GetCategoryByName(string name);
        Task<Category> GetCategoryById(long id);
        Task<IEnumerable<Category>> GetAll();
    }

    public class CategoryRepository : ICategoryRepository
    {
        private readonly CraftsmenDbContext _dbContext;

        public CategoryRepository(CraftsmenDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Category> GetCategoryByName(string name)
        {
            return await _dbContext.Category
                .Where(x => x.Name.Equals(name))
                .FirstOrDefaultAsync();
        }
        public async Task<Category> GetCategoryById(long id)
        {
            return await _dbContext.Category
                .Where(x => x.Id.Equals(id))
                .FirstOrDefaultAsync();
        }

        public async Task<long> Add(Category newCategory)
        {
            if (newCategory != null && (await GetCategoryByName(newCategory.Name)) == null)
            {
               _dbContext.Category.Add(newCategory);
                return newCategory.Id;
            }
            return -1L;
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            return await _dbContext.Category.Select(x => x).ToListAsync();
                
        }
    }
}
