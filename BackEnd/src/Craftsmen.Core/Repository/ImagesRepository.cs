﻿using Craftsmen.Database.Context;
using Craftsmen.Database.Entity.images;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Craftsmen.Core.Repository
{
    public interface IImagesRepository
    {
        Task<OfferImage> GetOfferImageById(long id);
        Task<bool> AddImage(OfferImage image);
    }

    public class ImagesRepository : IImagesRepository
    {
        private readonly CraftsmenDbContext _dbContext;

        public ImagesRepository(CraftsmenDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<OfferImage> GetOfferImageById(long id)
        {
            return await _dbContext.OfferImage
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<bool> AddImage(OfferImage image)
        {
            _dbContext.OfferImage.Add(image);
            return true;
        }
    }
}

