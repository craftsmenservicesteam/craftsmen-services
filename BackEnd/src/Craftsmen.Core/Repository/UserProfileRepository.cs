﻿using Craftsmen.Database.Context;
using Craftsmen.Database.Entity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Craftsmen.Core.Repository
{
    public interface IUserProfileRepository
    {
        Task<bool> Add(UserProfile userProfile, CraftsmanProfile craftsmanProfile);
        Task<UserProfile> GetUserProfileById(long id);
        Task<UserProfile> GetUserProfileByAuthId(string id);
        Task<CraftsmanProfile> GetCraftsmanProfileById(long id);
        Task<bool> Update(UserProfile userProfile, CraftsmanProfile craftsmanProfile);
    }

    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly CraftsmenDbContext _dbContext;

        public UserProfileRepository(CraftsmenDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Add(UserProfile userProfile,
            CraftsmanProfile craftsmanProfile)
        {
            if (userProfile != null)
            {
                await _dbContext.UserProfile.AddAsync(userProfile);
                if (userProfile.IsCraftsman && craftsmanProfile != null)
                {
                    craftsmanProfile.UserProfile = userProfile;
                    await _dbContext.CraftsmanProfile.AddAsync(craftsmanProfile);
                }
                await _dbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<UserProfile> GetUserProfileByAuthId(string id)
        {
            return await _dbContext.UserProfile
                .Where(x => x.AuthId == id)
                .FirstOrDefaultAsync();
        }

        public async Task<UserProfile> GetUserProfileById(long id)
        {
            return await _dbContext.UserProfile
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<CraftsmanProfile> GetCraftsmanProfileById(long userId)
        {
            return await _dbContext.CraftsmanProfile
                .Where(x => x.UserProfile.Id == userId)
                .FirstOrDefaultAsync();
        }

        public async Task<bool> Update(UserProfile userProfile, CraftsmanProfile craftsmanProfile)
        {
            if (userProfile != null)
            {
                if(userProfile.IsCraftsman && craftsmanProfile != null)
                {
                    craftsmanProfile.UserProfile = userProfile;
                    _dbContext.CraftsmanProfile.Update(craftsmanProfile);
                    await _dbContext.SaveChangesAsync();
                }
                _dbContext.UserProfile.Update(userProfile);
                await _dbContext.SaveChangesAsync();
                return true;
            }

            return false;
        }
    }
}
