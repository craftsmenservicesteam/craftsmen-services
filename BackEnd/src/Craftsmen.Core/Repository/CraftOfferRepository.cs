﻿using Craftsmen.Database.Context;
using Craftsmen.Database.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Craftsmen.Core.Model;

namespace Craftsmen.Core.Repository
{
    public interface ICraftOfferRepository
    {
        Task Add(CraftOffer offer);
        Task<IEnumerable<CraftOffer>> GetMany(int offset, int hits);
        Task<CraftOffer> GetById(int id);
        Task<IEnumerable<CraftOffer>> GetOffersByCraftsmanId(int craftsmanId);
        Task<IEnumerable<CraftOffer>> GetManyWithFiltersAsync(Filters filters);
        int GetAllOffersByFiltersCount(Filters filters);
    }

    public class CraftOfferRepository: ICraftOfferRepository
    {
        private readonly CraftsmenDbContext _dbContext;

        public CraftOfferRepository(CraftsmenDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Add(CraftOffer offer)
        {
            if (offer != null)
            {
                await _dbContext.CraftOffer.AddAsync(offer);
            }
        }

        public async Task<IEnumerable<CraftOffer>> GetMany(int offset, int hits)
        {
            return await _dbContext.CraftOffer
                .OrderByDescending(x => x.CreationDate)
                .Skip(offset)
                .Take(hits)
                .ToListAsync();
        }

        public async Task<CraftOffer> GetById(int id)
        {
            return await _dbContext.CraftOffer
                .Where(x => x.Id.Equals(id))
                .Include(x => x.Craftsman)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<CraftOffer>> GetManyWithFiltersAsync(Filters filters)
        {
            return await getAllOffersByFilters(filters)
                .OrderByDescending(x => x.CreationDate)
                .Skip(filters.Offset)
                .Take(filters.Hits)
                .ToListAsync();
        }

        public int GetAllOffersByFiltersCount(Filters filters)
        {
            return getAllOffersByFilters(filters)
                .Count();
        }

        public async Task<IEnumerable<CraftOffer>> GetOffersByCraftsmanId(int id)
        {
            return await _dbContext.CraftOffer
                .Where(x => x.CraftsmanId.Equals(id))
                .OrderByDescending(x => x.CreationDate)
                .ToListAsync();
        }

        private IQueryable<CraftOffer> getAllOffersByFilters(Filters filters)
        {
            var result = _dbContext.CraftOffer
                .Where(x => x.CategoryId > -1);

            result = filters.CategoryId != 0
                ? result.Where(x => x.CategoryId.Equals(filters.CategoryId))
                : result;

            result = filters.Location != ""
                ? result.Where(x => x.AvailableLocation.Contains(filters.Location.ToLower())
                        || x.AvailableLocation.Contains(filters.Location.First().ToString().ToUpper() + filters.Location.Substring(1)))
                : result;

            result = filters.Contains != ""
                ? result.Where(x => filters.Contains == null ? true : x.Description.Contains(filters.Contains) || x.Title.Contains(filters.Contains))
                : result;

            return result;
        }
    }
}
