﻿using Craftsmen.Database.Context;
using Craftsmen.Database.Entity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Craftsmen.Core.Repository
{
    public interface IOpinionRepository
    {
        Task<bool> Add(Opinion opinion);
        Task<IEnumerable<Opinion>> GetManyByCraftsmanId(long id);
    }

    public class OpinionRepository : IOpinionRepository
    {
        private readonly CraftsmenDbContext _dbContext;

        public OpinionRepository(CraftsmenDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Add(Opinion opinion)
        {
            if (opinion != null)
            {
                await _dbContext.Opinion.AddAsync(opinion);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<Opinion>> GetManyByCraftsmanId(long id)
        {
            return await _dbContext.Opinion
                .Where(x => x.MarkedCraftsmanId == id)
                .ToListAsync();
        }
    }
}
