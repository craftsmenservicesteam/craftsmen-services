﻿using Craftsmen.Database.Context;
using Craftsmen.Database.Entity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Craftsmen.Core.Repository
{
    public interface ISubcategoryRepository
    {
        Task<long> Add(Subcategory subcategory);
        Task<Subcategory> GetSubcategoryByName(string name);
        Task<Subcategory> GetSubcategoryById(long id);
        Task<IEnumerable<Subcategory>> GetAll();
    }

    public class SubcategoryRepository : ISubcategoryRepository
    {
        private readonly CraftsmenDbContext _dbContext;

        public SubcategoryRepository(CraftsmenDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Subcategory> GetSubcategoryByName(string name)
        {
            return await _dbContext.Subcategory
                .Where(x => x.Name.Equals(name))
                .FirstOrDefaultAsync();
        }

        public async Task<Subcategory> GetSubcategoryById(long id)
        {
            return await _dbContext.Subcategory
             .Where(x => x.Id.Equals(id))
             .FirstOrDefaultAsync();
        }

        public async Task<long> Add(Subcategory newSubcategory)
        {
            if (newSubcategory != null && (await GetSubcategoryByName(newSubcategory.Name)) == null)
            {
                _dbContext.Subcategory.Add(newSubcategory);
                return newSubcategory.Id;
            }
            return -1L;
        }

        public async Task<IEnumerable<Subcategory>> GetAll()
        {
            return await _dbContext.Subcategory.Select(x => x).ToListAsync();

        }
    }
}
