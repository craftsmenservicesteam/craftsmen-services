﻿using Craftsmen.Core.Repository;
using System.Threading.Tasks;
using Craftsmen.Database.Entity;
using System.Collections.Generic;
using AutoMapper;
using Craftsmen.Contract.DTO.Categories;

namespace Craftsmen.Core.Services.Offer
{
    public interface IGetCategoriesService
    {
        Task<GetCategoriesDTO[]> GetAllAsync();
    }

    public class GetCategoriesService : IGetCategoriesService
    {

        private readonly ICategoryRepository _categoryRepo;

        public GetCategoriesService(ICategoryRepository categoryRepo)
        {
            _categoryRepo = categoryRepo;
        }

        public async Task<GetCategoriesDTO[]> GetAllAsync()
        {
            var categories = Mapper.Map<IEnumerable<Category>>(await _categoryRepo.GetAll());

            List<GetCategoriesDTO> categoriesDTOsList = new List<GetCategoriesDTO>();

            foreach(Category cat in categories)
            {
                categoriesDTOsList.Add(new GetCategoriesDTO()
                {
                    Id = cat.Id,
                    Name = cat.Name
                });
            }

            return categoriesDTOsList.ToArray();
            
        }
    }
}
