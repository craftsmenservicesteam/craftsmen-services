﻿using AutoMapper;
using Craftsmen.Contract.DTO.Offer;
using Craftsmen.Core.Model;
using Craftsmen.Core.Repository;
using Craftsmen.Core.Services.UserProfileService;
using Craftsmen.Database.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Craftsmen.Core.Services.Offer
{
    public interface IGetCraftOfferService
    {
        Task<GetManyCraftOffersDTO> GetMany(int offset, int hits);
        Task<GetOneCraftOfferDTO> GetOne(int id);
        Task<GetManyCraftOffersDTO> GetManyWithFiltersAsync(Filters filters);
        Task<GetManyCraftOffersDTO> GetOffersByCraftsmanId(int id);
    }

    public class GetCraftOfferService : IGetCraftOfferService
    {
        private readonly ICraftOfferRepository _offerRepo;
        private readonly ICategoryRepository _categoryRepo;
        private readonly ISubcategoryRepository _subcategoryRepo;
        private readonly IUserProfileService _userProfileService;
        private readonly IImagesRepository _imagesRepository;

        public GetCraftOfferService(
            ICraftOfferRepository offerRepo,
            ICategoryRepository categoryRepo,
            ISubcategoryRepository subcategoryRepo,
            IUserProfileService userProfileService,
            IImagesRepository imagesRepository
            )
        {
            _offerRepo = offerRepo;
            _categoryRepo = categoryRepo;
            _subcategoryRepo = subcategoryRepo;
            _userProfileService = userProfileService;
            _imagesRepository = imagesRepository;
        }

        public async Task<GetManyCraftOffersDTO> GetMany(int offset, int hits)
        {
            var offerEntities = await _offerRepo.GetMany(offset, hits);
            var offers = Mapper.Map<IEnumerable<GetCraftOffersDTO>>(offerEntities);

            for (int i = 0; i < offerEntities.Count(); i++)
            {
                offers.ElementAt(i).Image =
                    (await _imagesRepository.GetOfferImageById(offerEntities.ElementAt(i).ImageId)).Image;
            }

            return new GetManyCraftOffersDTO()
            {
                Offers = offers,
                Count = offers.Count()
            };      
        }

        public async Task<GetManyCraftOffersDTO> GetManyWithFiltersAsync(Filters filters)
        {
            var offerEntities = await _offerRepo.GetManyWithFiltersAsync(filters);

            var offers = Mapper.Map<IEnumerable<GetCraftOffersDTO>>(offerEntities);

            for (int i = 0; i < offerEntities.Count(); i++)
            {
                offers.ElementAt(i).Image =
                    (await _imagesRepository.GetOfferImageById(offerEntities.ElementAt(i).ImageId)).Image;
            }


            return new GetManyCraftOffersDTO()
            {
                Offers = offers,
                Count = _offerRepo.GetAllOffersByFiltersCount(filters)
            };
        }

        public async Task<GetOneCraftOfferDTO> GetOne(int id)
        {
            var foundOffer = await _offerRepo.GetById(id);

            var offerDTO = Mapper.Map<GetOneCraftOfferDTO>(foundOffer);

            Category foundCategory = await _categoryRepo.GetCategoryById(foundOffer.CategoryId);
            Subcategory foundSubcategory = await _subcategoryRepo.GetSubcategoryById(foundOffer.SubcategoryId);

            offerDTO.CategoryName = foundCategory.Name;
            offerDTO.SubcategoryName = foundSubcategory.Name;

            offerDTO.Craftsman = await _userProfileService
                .GetUserProfileById(foundOffer.Craftsman.UserProfileId);

            offerDTO.Image = (await _imagesRepository.GetOfferImageById(foundOffer.ImageId)).Image;

            return offerDTO;
            
        }

        public async Task<GetManyCraftOffersDTO> GetOffersByCraftsmanId(int id)
        {
            var offerEntities = await _offerRepo.GetOffersByCraftsmanId(id);
            var offers = Mapper.Map<IEnumerable<GetCraftOffersDTO>>(offerEntities);
            for (int i = 0; i < offerEntities.Count(); i++)
            {
                offers.ElementAt(i).Image =
                    (await _imagesRepository.GetOfferImageById(offerEntities.ElementAt(i).ImageId)).Image;
            }
            return new GetManyCraftOffersDTO()
            {
                Offers = offers,
                Count = offers.Count()
            };
       }
    }
}
