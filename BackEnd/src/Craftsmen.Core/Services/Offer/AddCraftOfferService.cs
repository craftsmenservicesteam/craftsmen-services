﻿using AutoMapper;
using Craftsmen.Contract.DTO.Offer;
using Craftsmen.Core.Repository;
using Craftsmen.Database.Context;
using Craftsmen.Database.Entity;
using Craftsmen.Database.Entity.images;
using System;
using System.Threading.Tasks;

namespace Craftsmen.Core.Services.Offer
{
    public interface IAddCraftOfferService
    {
        Task<bool> Add(AddCraftOfferDTO dto, string authId);
    }

    public class AddCraftOfferService : IAddCraftOfferService
    {
        private readonly ICraftOfferRepository _offerRepo;
        private readonly ICategoryRepository _categoryRepo;
        private readonly ISubcategoryRepository _subcategoryRepo;
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IImagesRepository _imagesRepository;
        private readonly CraftsmenDbContext _dbContext;

        public AddCraftOfferService(
            CraftsmenDbContext context,
            ICraftOfferRepository offerRepo,
            ICategoryRepository categoryRepo,
            ISubcategoryRepository subcategoryRepo,
            IUserProfileRepository userProfileRepository,
            IImagesRepository imagesRepository)
        {
            _offerRepo = offerRepo;
            _dbContext = context;
            _categoryRepo = categoryRepo;
            _subcategoryRepo = subcategoryRepo;
            _userProfileRepository = userProfileRepository;
            _imagesRepository = imagesRepository;
        }

        public async Task<bool> Add(AddCraftOfferDTO offer, string authId)
        {
            var isSaved = false;

            var newOffer = Mapper.Map<CraftOffer>(offer);
            newOffer.Craftsman = await _userProfileRepository
                .GetCraftsmanProfileById((await _userProfileRepository.GetUserProfileByAuthId(authId)).Id);

            newOffer.Category = await _categoryRepo.GetCategoryById(offer.CategoryId);
            newOffer.Subcategory = await _subcategoryRepo.GetSubcategoryById(offer.SubcategoryId);

            await _offerRepo.Add(newOffer);
            await _imagesRepository.AddImage(new OfferImage() { Image = offer.ImageBase64, Offer = newOffer });

            isSaved = await _dbContext.SaveChangesAsync() > 0;

            return isSaved;
        }
    }
}
