﻿using AutoMapper;
using Craftsmen.Contract.DTO.Categories;
using Craftsmen.Core.Repository;
using Craftsmen.Database.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Craftsmen.Core.Services.Offer
{
    public interface IGetSubcategoriesService
    {
        Task<GetSubcategoriesDTO[]> GetAllAsync();
    }

    public class GetSubcategoryService : IGetSubcategoriesService
    {
        private readonly ISubcategoryRepository _subcategoryRepo;

        public GetSubcategoryService(ISubcategoryRepository subcategoryRepo)
        {
            _subcategoryRepo = subcategoryRepo;
        }

        public async Task<GetSubcategoriesDTO[]> GetAllAsync()
        {
            var subcategories = Mapper.Map<ICollection<Subcategory>>(await _subcategoryRepo.GetAll());

            List<GetSubcategoriesDTO> subcategoriesDTOsList = new List<GetSubcategoriesDTO>();

            foreach (Subcategory sub in subcategories)
            {
                subcategoriesDTOsList.Add(new GetSubcategoriesDTO()
                {
                    Id = sub.Id,
                    Name = sub.Name,
                    CategoryId = sub.CategoryId
                });
            }

            return subcategoriesDTOsList.ToArray();
        }
    }
}
