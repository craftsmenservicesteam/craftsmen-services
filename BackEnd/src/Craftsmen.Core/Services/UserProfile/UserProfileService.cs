﻿using AutoMapper;
using Craftsmen.Contract.DTO.UserProfile;
using Craftsmen.Core.Repository;
using Craftsmen.Core.Utils;
using Craftsmen.Database.Entity;
using System.Threading.Tasks;

namespace Craftsmen.Core.Services.UserProfileService
{
    public interface IUserProfileService
    {
        Task<bool> AddMyProfile(CraftsmanProfileDTO profile, string authId);
        Task<CraftsmanProfileDTO> GetUserProfileById(long id);
        Task<CraftsmanProfileDTO> GetMyProfile(string authId);
        Task<bool> UpdateMyProfile(CraftsmanProfileDTO profile, string authId);
    }

    public class UserProfileService: IUserProfileService
    {
        private readonly IUserProfileRepository _repository;

        public UserProfileService(
            IUserProfileRepository repository)
        {
            _repository = repository;
        }

        public async Task<bool> AddMyProfile(CraftsmanProfileDTO profile, string authId)
        {
            if (await _repository.GetUserProfileByAuthId(authId) == null)
            {
                var craftsmanProfile = profile.IsCraftsman
                    ? Mapper.Map<CraftsmanProfile>(profile)
                    : null;

                var userProfile = Mapper.Map<UserProfile>(profile);
                userProfile.AuthId = authId;


                return await _repository.Add(userProfile, craftsmanProfile);
            }
            return false;
        }

        public async Task<CraftsmanProfileDTO> GetUserProfileById(long id)
        {
            return await getCraftsmanProfileDTOByUserProfile(await _repository.GetUserProfileById(id));
        }

        public async Task<CraftsmanProfileDTO> GetMyProfile(string authId)
        {
            return await getCraftsmanProfileDTOByUserProfile(await _repository.GetUserProfileByAuthId(authId));
        }

        private async Task<CraftsmanProfileDTO> getCraftsmanProfileDTOByUserProfile(UserProfile userProfile)
        {
            if (userProfile == null)
                return null;

            var profile = Mapper.Map<CraftsmanProfileDTO>(userProfile);
            if (userProfile.IsCraftsman)
            {
                var craftsmanProfile = Mapper.Map<CraftsmanProfileDTO>(
                    await _repository.GetCraftsmanProfileById(userProfile.Id));

                PropertiesHelper.CopyProperties(craftsmanProfile, profile);
                PropertiesHelper.CopyProperties(profile, craftsmanProfile);
            }

            return profile;
        }

        public async Task<bool> UpdateMyProfile(CraftsmanProfileDTO profile, string authId)
        {
            var craftsmanProfile = profile.IsCraftsman
                ? Mapper.Map<CraftsmanProfile>(profile)
                : null;

            var userProfile = Mapper.Map<UserProfile>(profile);

            userProfile.AuthId = authId;

            return await _repository.Update(userProfile, craftsmanProfile);          
        }
    }
}
