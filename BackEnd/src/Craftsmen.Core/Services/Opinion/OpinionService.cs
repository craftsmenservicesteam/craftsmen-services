﻿using AutoMapper;
using Craftsmen.Contract.DTO.Opinion;
using Craftsmen.Core.Repository;
using Craftsmen.Database.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Craftsmen.Core.Services.Opinions
{
    public interface IOpinionService
    {
        Task<bool> Add(AddOpinionDTO opinion, string authId);
        Task<IEnumerable<OpinionDTO>> GetManyByCraftsmanId(long id);
    }

    public class OpinionService : IOpinionService
    {
        private readonly IOpinionRepository _opinionRepository;
        private readonly IUserProfileRepository _userProfileRepository;

        public OpinionService(
            IOpinionRepository opinionRepository,
            IUserProfileRepository userProfileRepository)
        {
            _opinionRepository = opinionRepository;
            _userProfileRepository = userProfileRepository;
        }

        public async Task<bool> Add(AddOpinionDTO newOpinion, string authId)
        {
            var opinion = Mapper.Map<Opinion>(newOpinion);

            opinion.MarkedCraftsman = await _userProfileRepository
                .GetCraftsmanProfileById(newOpinion.MarkedCraftsmanId);

            opinion.RaterUser = await _userProfileRepository
                .GetUserProfileByAuthId(authId);

            return await _opinionRepository.Add(opinion);
        }

        public async Task<IEnumerable<OpinionDTO>> GetManyByCraftsmanId(long id)
        {
            return Mapper.Map<IEnumerable<OpinionDTO>>(
                await _opinionRepository.GetManyByCraftsmanId(id));
        }
    }
}
