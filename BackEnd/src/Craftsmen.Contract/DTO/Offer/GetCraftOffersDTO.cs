﻿using Craftsmen.Database.Entity;
using System;

namespace Craftsmen.Contract.DTO.Offer
{
    public class GetCraftOffersDTO
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        public Category Category { get; set; }
        public Subcategory Subcategory { get; set; }
        public bool DoorToDoor { get; set; }
        public bool HasOwnTools { get; set; }
        public string AvailableLocation { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public float PricePerHour { get; set; }
        public string Image { get; set; }
    }
}
