﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Contract.DTO.Offer
{
    public class AddCraftOfferDTO
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime CreationDate { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int SubcategoryId { get; set; }
        [Required]
        public string AvailableLocation { get; set; }
        [Required]
        public float Price { get; set; }
        public float PricePerHour { get; set; }
        public bool DoorToDoor { get; set; }
        public bool HasOwnTools { get; set; }
        public string ImageBase64 { get; set; }
    }
}
