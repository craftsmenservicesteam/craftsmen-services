﻿using Craftsmen.Contract.DTO.UserProfile;
using System;

namespace Craftsmen.Contract.DTO.Offer
{
    public class GetOneCraftOfferDTO
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public string CategoryName { get; set; }
        public string SubcategoryName { get; set; }
        public string AvailableLocation { get; set; }
        public float Price { get; set; }
        public float PricePerHour { get; set; }
        public bool DoorToDoor { get; set; }
        public bool HasOwnTools { get; set; }
        public string Image { get; set; }
        public CraftsmanProfileDTO Craftsman { get; set; }
    }
}
