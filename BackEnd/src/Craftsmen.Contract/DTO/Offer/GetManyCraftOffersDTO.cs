﻿using System.Collections.Generic;

namespace Craftsmen.Contract.DTO.Offer
{
    public class GetManyCraftOffersDTO
    {
        public IEnumerable<GetCraftOffersDTO> Offers { get; set; }
        public int Count { get; set; }
    }
}
