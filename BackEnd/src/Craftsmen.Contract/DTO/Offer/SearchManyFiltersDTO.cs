﻿namespace Craftsmen.Contract.DTO.Offer
{
    public class SearchManyFiltersDTO
    {
        public int offset { get; set; }
        public int hits { get; set; }
    }
}
