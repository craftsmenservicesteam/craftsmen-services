﻿using System;

namespace Craftsmen.Contract.DTO.Opinion
{
    public class OpinionDTO
    {
        public int Rate { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
