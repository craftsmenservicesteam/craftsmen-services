﻿namespace Craftsmen.Contract.DTO.Opinion
{
    public class AddOpinionDTO: OpinionDTO
    {
        public long MarkedCraftsmanId { get; set; }
    }
}
