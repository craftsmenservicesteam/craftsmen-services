﻿using System.ComponentModel.DataAnnotations;

namespace Craftsmen.Contract.DTO.UserProfile
{
    public class UserProfileDTO
    {
        public long Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Surname { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }
        public bool IsCraftsman { get; set; }
    }
}
