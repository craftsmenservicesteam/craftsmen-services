﻿using Craftsmen.Contract.DTO.Shared;

namespace Craftsmen.Contract.DTO.UserProfile
{
    public class CraftsmanProfileDTO: UserProfileDTO
    {
        public string Company { get; set; }
        public string Description { get; set; }
        public string PhoneNumber { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public long? CraftsmanId { get; set; }
        public string Business { get; set; }
        public double? Rate { get; set; }

        public TimeRangeDTO Monday { get; set; }
        public TimeRangeDTO Tuesday { get; set; }
        public TimeRangeDTO Wednesday { get; set; }
        public TimeRangeDTO Thursday { get; set; }
        public TimeRangeDTO Friday { get; set; }
        public TimeRangeDTO Saturday { get; set; }
        public TimeRangeDTO Sunday { get; set; }
    }
}
