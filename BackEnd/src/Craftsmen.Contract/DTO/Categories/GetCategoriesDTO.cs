﻿namespace Craftsmen.Contract.DTO.Categories
{
    public class GetCategoriesDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
