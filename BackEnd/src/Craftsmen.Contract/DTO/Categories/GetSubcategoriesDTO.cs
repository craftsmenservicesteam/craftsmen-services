﻿namespace Craftsmen.Contract.DTO.Categories
{
    public class GetSubcategoriesDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CategoryId { get; set; }
    }
}
