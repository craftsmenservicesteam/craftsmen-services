﻿using Newtonsoft.Json;

namespace Craftsmen.Contract.DTO.Shared
{
    public class TimeRangeDTO
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
