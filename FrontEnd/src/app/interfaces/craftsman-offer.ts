export interface CraftsmanOfferImage {
  alt: string;
  src: string;
}

export interface CraftsmanOffer {
  title: string;
  description: string;
  price: number;
  pricePerHour: number;
  images: Array<CraftsmanOfferImage>;
}

export interface CraftsmanOffersList {
  offers: Array<CraftsmanOffer>;
}
