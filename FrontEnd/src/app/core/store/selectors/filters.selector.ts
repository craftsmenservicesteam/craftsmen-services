import { createSelector } from '@ngrx/store';
import { IAppState } from "../state/app.state";
import { IFiltersState } from '../state/filters.state';

const filters = (state: IAppState) => state.filters;

export const selectFilters = createSelector(
    filters,
    (state: IFiltersState) => state
);
