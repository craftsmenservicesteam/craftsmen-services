import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { IMyProfileState } from '../state/my-profile.state';

const myProfile = (state: IAppState) => state.myProfile;

export const selectMyProfile = createSelector(
    myProfile,
    (state: IMyProfileState) => state
);
