import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { ILoginState } from '../state/login.state';

const login = (state: IAppState) => state.login;

export const selectIsLoggedIn = createSelector(
    login,
    (state: ILoginState) => state.isLogedIn
);
