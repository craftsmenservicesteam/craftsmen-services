import { IFilters } from '../../../contracts/filters.interface';

export interface IFiltersState extends IFilters {

}

export const initialFiltersSate: IFiltersState = {
    offset: 0,
    hits: 5,
    categoryId: 0
};
