export interface ILoginState {
    isLogedIn: boolean;
}

export const initialLoginSate: ILoginState = {
    isLogedIn: false
};
