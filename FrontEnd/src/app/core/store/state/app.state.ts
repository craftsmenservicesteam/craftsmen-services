import { IMyProfileState } from './my-profile.state';
import { ILoginState } from './login.state';
import { IFiltersState } from './filters.state';

export interface IAppState {
    login: ILoginState,
    myProfile: IMyProfileState,
    filters: IFiltersState
}
