import { createAction, props } from '@ngrx/store';
import { IFilters } from '../../../contracts/filters.interface';

export enum FiltersActions {
    SetFilters = '[Filters] SetFilters'
}

export const setFilters = createAction(
    FiltersActions.SetFilters,
    props<{filters: IFilters}>()
);
