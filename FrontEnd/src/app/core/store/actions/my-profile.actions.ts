import { createAction, props } from '@ngrx/store';
import { ICraftsmanProfile } from '../../../modules/user-profile/interfaces/craftsman-profile.interface';

export enum MyProfileActions {
    Get = '[My Profile] Get',
    GetSuccess = "[My Profile] GetSuccess"
}

export const getMyProfile = createAction(
    MyProfileActions.Get,
);

export const getMyProfileSuccess = createAction(
    MyProfileActions.GetSuccess,
    props<{profile: ICraftsmanProfile}>()
);
