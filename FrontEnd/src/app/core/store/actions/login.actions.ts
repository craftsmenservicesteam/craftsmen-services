import { createAction, props } from '@ngrx/store';

export enum LoginActions {
    SetIsLoggedIn = '[Login] SetIsLoggedIn'
}

export const setIsLoggedIn = createAction(
    LoginActions.SetIsLoggedIn,
    props<{isLoggedIn: boolean}>()
);
