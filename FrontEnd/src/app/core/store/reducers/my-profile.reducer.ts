import { createReducer, on } from "@ngrx/store";
import * as MyProfileActions from '../actions/my-profile.actions';

export const myProfileReducer = createReducer(
    null,
    on(MyProfileActions.getMyProfileSuccess, (state, myProfile) => ({...myProfile.profile}))
);
