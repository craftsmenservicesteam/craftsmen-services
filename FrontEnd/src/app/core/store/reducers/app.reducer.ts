import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { myProfileReducer } from './my-profile.reducer';
import { loginReducer } from './login.reducer';
import { environment } from '../../../../environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';
import { filtersReducer } from './filters.reducer';

export const metaReducers: MetaReducer<IAppState>[] = !environment.production ? [storeFreeze]: [];

export const appReducer: ActionReducerMap<IAppState, any> = {
    myProfile: myProfileReducer,
    login: loginReducer,
    filters: filtersReducer
}
