import { createReducer, on } from "@ngrx/store";
import * as LoginActions from '../actions/login.actions';
import { initialLoginSate } from '../state/login.state';

export const loginReducer = createReducer(
    initialLoginSate,
    on(LoginActions.setIsLoggedIn, (state, login) => ({
        ...state, isLogedIn: login.isLoggedIn
    }))
);
