import { createReducer, on } from "@ngrx/store";
import { initialFiltersSate } from '../state/filters.state';
import * as FiltersActions from '../actions/filters.actions';

export const filtersReducer = createReducer(
    initialFiltersSate,
    on(FiltersActions.setFilters, (state, filters) => ({
        ...filters.filters
    }))
);
