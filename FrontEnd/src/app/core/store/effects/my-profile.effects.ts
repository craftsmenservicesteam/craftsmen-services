import { Injectable } from "@angular/core";
import { UserProfileHttpService } from '../../http/user-profile-http.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MyProfileActions } from '../actions/my-profile.actions';
import { mergeMap, map, take, filter } from 'rxjs/operators';

@Injectable()
export class MyProfileEffects {
    getMyProfile$ = createEffect(() => this.actions$.pipe(
        ofType(MyProfileActions.Get),
        mergeMap(() => this.userProfileHttp.GetMyProfile().pipe(
            filter(x => x.succeeded),
            map(res => ({
                type: MyProfileActions.GetSuccess,
                profile: res.body
            }))
        ))
    ));

    constructor(
        private actions$: Actions,
        private userProfileHttp: UserProfileHttpService
    ) { }
}
