import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddOfferFormComponent } from '../modules/addOffer/components/addOfferForm/add-offer-form.component';
import { CraftsmanProfilePageComponent } from '../modules/craftsman-profile/pages/craftsman-profile-page/craftsman-profile-page.component';
import { MyProfileComponent } from '../modules/user-profile/pages/my-profile/my-profile.component';
import { WaitForLoginComponent } from './authentication/auth/wait-for-login.component';
import { OffersListComponent } from '../modules/offer/pages/offersList/offers-list.component';
import { OfferPreviewComponent } from '../modules/offer/pages/offerPreview/offer-preview.component';
import { WelcomePageComponent } from '../modules/welcome-page/welcome-page.component';

const routes: Routes = [
  { path: '', component: OffersListComponent },
  { path: 'offers/add', component: AddOfferFormComponent },
  { path: 'offers/show', component: OffersListComponent },
  { path: 'user', component: CraftsmanProfilePageComponent},
  { path: 'myProfile', component: MyProfileComponent },
  { path: 'login', component: WaitForLoginComponent },
  { path: 'offer/:id', component: OfferPreviewComponent },
  { path: 'welcome', component: WelcomePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
