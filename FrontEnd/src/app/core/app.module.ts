import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { AddOfferModule } from '../modules/addOffer/add-offer.module';
import { CraftsmanProfileModule } from '../modules/craftsman-profile/craftsman-profile.module';
import { OfferHttpService } from './http/offer-http.service';
import { HttpClientModule } from '@angular/common/http';
import { UrlResolver } from './services/url.resolver';
import { UserProfileModule } from '../modules/user-profile/user-profile.module';
import { UserProfileHttpService } from './http/user-profile-http.service';
import { SharedModule } from '../shared/shared.module';
import { AuthService } from './authentication/auth/auth.service';
import { StoreModule } from '@ngrx/store';
import { appReducer, metaReducers } from './store/reducers/app.reducer';
import { EffectsModule } from '@ngrx/effects';
import { appEffects } from './store/effects/app.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { WaitForLoginComponent } from './authentication/auth/wait-for-login.component';
import {
  MatCardModule,
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { OffersModule } from '../modules/offer/offers.module';
import { WelcomePageModule } from '../modules/welcome-page/welcome-page.module';
import { OpinionHttpService } from './http/opinion.http.service';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    MenuComponent,
    WaitForLoginComponent,
  ],
    imports: [
        HttpClientModule,
        BrowserModule,
        AppRoutingModule,
        AddOfferModule,
        OffersModule,
        CraftsmanProfileModule,
        SharedModule,
        UserProfileModule,
        StoreModule.forRoot(appReducer, {metaReducers}),
        EffectsModule.forRoot(appEffects),
        StoreDevtoolsModule.instrument(),
        MatCardModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        FormsModule,
        WelcomePageModule
    ],
  providers: [
    UrlResolver,
    OfferHttpService,
    UserProfileHttpService,
    AuthService,
    OpinionHttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
