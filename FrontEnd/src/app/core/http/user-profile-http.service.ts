import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlResolver } from '../services/url.resolver';
import { ICraftsmanProfile } from '../../modules/user-profile/interfaces/craftsman-profile.interface';
import { Observable } from 'rxjs';
import { IResult } from './result.interface';
import { getHttpOptions } from './authorization-header';

@Injectable()
export class UserProfileHttpService {

  constructor(
    private http: HttpClient,
    private urlResolver: UrlResolver
  ) { }

  public GetUserProfileById(
      id: number
  ): Observable<IResult<ICraftsmanProfile>> {
    return this.http.get<IResult<ICraftsmanProfile>>(
      this.urlResolver.resolve(`UserProfile/GetUserProfileById/${id}`)
    );
  }

  public AddMyProfile(
    profile: ICraftsmanProfile
  ): Observable<IResult<boolean>> {
    return this.http.post<IResult<boolean>>(
      this.urlResolver.resolve('UserProfile/AddMyProfile'),
      profile,
      getHttpOptions()
    );
  }

  public GetMyProfile(): Observable<IResult<ICraftsmanProfile>> {
    return this.http.get<IResult<ICraftsmanProfile>>(
      this.urlResolver.resolve(`UserProfile/GetMyProfile`), getHttpOptions()
    );
  }

  public UpdateMyProfile(
    profile: ICraftsmanProfile
  ): Observable<IResult<boolean>> {
    return this.http.put<IResult<boolean>>(
      this.urlResolver.resolve('UserProfile/UpdateMyProfile'),
      profile,
      getHttpOptions()
    );
  }
}
