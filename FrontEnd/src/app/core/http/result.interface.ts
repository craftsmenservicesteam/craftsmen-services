export interface IResult <T> {
    succeeded: boolean;
    body: T;
}
