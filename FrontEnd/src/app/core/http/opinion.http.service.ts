import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { UrlResolver } from '../services/url.resolver';
import { Observable } from 'rxjs';
import { IAddOpinionDTO } from '../../contracts/add-opinion-dto.interface';
import { getHttpOptions } from './authorization-header';
import { IOpinionDTO } from '../../contracts/opinion-dto.interface';
import { IResult } from './result.interface';

@Injectable()
export class OpinionHttpService {

    constructor(
        private http: HttpClient,
        private urlResolver: UrlResolver
    ) { }

    public Add(
        opinion: IAddOpinionDTO
    ): Observable<IResult<boolean>> {
        return this.http.post<IResult<boolean>>(
            this.urlResolver.resolve('Opinion/Add'),
            opinion,
            getHttpOptions()
        );
    }

    public GetByCraftsmanId(
        id: number
    ) {
        return this.http.get<IResult<IOpinionDTO[]>>(
            this.urlResolver.resolve(`Opinion/GetMany/${id}`)
        );
    }
}
