import { HttpHeaders } from '@angular/common/http';

export function getHttpOptions(): object {
  const token = localStorage.getItem('access_token');

  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': `${token}`
    })
  };

  httpOptions.headers =
  httpOptions.headers.set('Authorization', `Bearer ${token}`);
  return httpOptions;
}
