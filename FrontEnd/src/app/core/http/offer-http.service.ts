import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlResolver } from '../services/url.resolver';
import { IAddOfferDTO } from '../../contracts/add-offer-dto.interface';
import { Observable } from 'rxjs';
import { IGetManyCraftOffersDTO } from '../../contracts/get-many-craft-offer-dto';
import { Category, Subcategory } from '../../modules/addOffer/components/addOfferForm/add-offer-form.component';
import { getHttpOptions } from './authorization-header';
import { IResult } from './result.interface';
import { IFilters } from '../../contracts/filters.interface';

@Injectable()
export class OfferHttpService {

  constructor(
    private http: HttpClient,
    private urlResolver: UrlResolver
  ) { }
  public Add(
    offer: IAddOfferDTO
  ): Observable<boolean> {
    return this.http.post<boolean>(
      this.urlResolver.resolve('CraftOffer/Add'),
      offer,
      getHttpOptions()
    );
  }
  public SearchMany(
    offset: number,
    hits: number
  ): Observable<IGetManyCraftOffersDTO> {
    return this.http.post<IGetManyCraftOffersDTO>(
      this.urlResolver.resolve('CraftOffer/SearchMany'),
      {offset, hits}
    );
  }

  public GetCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.urlResolver.resolve('CraftOffer/GetCategories'));
  }

  public GetSubcategories(): Observable<Subcategory[]> {
    return this.http.get<Subcategory[]>(this.urlResolver.resolve('CraftOffer/GetSubcategories'));
  }

  public GetOfferById(offerId: number): Observable<any> {
      return this.http.get<any>(this.urlResolver.resolve('CraftOffer/GetOfferById/' + offerId));
  }

  public GetOffersByCraftsmanId(craftsmanId: number): Observable<IResult<IGetManyCraftOffersDTO>> {
    return this.http.get<IResult<IGetManyCraftOffersDTO>>(this.urlResolver.resolve(`/CraftOffer/GetOffersByCraftsmanId/${craftsmanId}`));
  }

  public GetManyWithFilters(
    filters: IFilters
  ) {
    let queryParams = `?category=${filters.categoryId}`;

    queryParams = filters.contains
      ? queryParams + `&contains=${filters.contains}`
      : queryParams;

    queryParams = filters.location
      ? queryParams + `&location=${filters.location}`
      : queryParams;

    return this.http.get<IResult<IGetManyCraftOffersDTO>>(this.urlResolver.resolve(
      `/CraftOffer/GetManyWithFilters/${filters.offset}/${filters.hits}${queryParams}`)
    );
  }
}
