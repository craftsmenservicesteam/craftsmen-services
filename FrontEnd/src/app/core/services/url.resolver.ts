import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';

@Injectable()
export class UrlResolver {

    public resolve(
        route: string
    ): string {
        return `${environment.baseUrl}/${route}`;
    }
}
