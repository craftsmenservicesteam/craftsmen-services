import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../store/state/app.state';
import { selectIsLoggedIn } from '../store/selectors/login.selectors';
import { AuthService } from '../authentication/auth/auth.service';
import { IMyProfileState } from '../store/state/my-profile.state';
import { selectMyProfile } from '../store/selectors/my-profile.selectors';

@Component({
  selector: 'app-craftsmen-menu',
  templateUrl: 'menu.component.html',
  styleUrls: ['menu.component.scss']
})
export class MenuComponent {
  @Input() mobileQuery: MediaQueryList;

  public isLoggedIn$: Observable<boolean>;
  public myProfile$: Observable<IMyProfileState>;

  constructor(
    public auth: AuthService,
    private store: Store<IAppState>
  ) {
    this.isLoggedIn$ = this.store.pipe(select(selectIsLoggedIn));
    this.myProfile$ = this.store.pipe(select(selectMyProfile));
  }
}
