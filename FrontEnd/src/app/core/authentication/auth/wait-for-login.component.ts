import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'wait-for-login',
  template: `
    <p>
      Loading...
    </p>
  `,
  styles: []
})
export class WaitForLoginComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.beginLoggedInState();
  }
}
