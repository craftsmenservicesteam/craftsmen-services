// source: https://github.com/andaro74/Angular-5-Net-Core-Web-API-with-Auth0
import { Injectable } from '@angular/core';
import auth0 from 'auth0-js';
import { config } from '../../../config/auth_config';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { IAppState } from '../../store/state/app.state';
import { setIsLoggedIn } from '../../store/actions/login.actions';
import { getMyProfile, getMyProfileSuccess } from '../../store/actions/my-profile.actions';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // Create an observable of Auth0 instance of client
  private auth0Client = new auth0.WebAuth({
    clientID: config.clientId,
    domain: config.domain,
    responseType: 'token id_token',
    audience: config.apiUrl,
    redirectUri: config.callbackUrl,
    scope: 'openid profile email read:messages'
  });
  private expiresAt;

  constructor(
    private router: Router,
    private store: Store<IAppState>
  ) {
    
  }

  public login(): void {
    this.auth0Client.authorize();
  }

  public beginLoggedInState(): void {
    this.auth0Client.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        this.store.dispatch(setIsLoggedIn({isLoggedIn: true}))
        this.store.dispatch(getMyProfile());
        this.handleLoginState;
        this.router.navigate(['/']);
      }
    });
  }

  private setSession(authResult): void {
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    this.expiresAt = JSON.parse(localStorage.getItem('expires_at') || '{}');
  }

  public isTokenNotExpired(): boolean {
    return Date.now() < this.expiresAt;
  }

  public logout(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    this.store.dispatch(getMyProfileSuccess({profile: null}))
    this.store.dispatch(setIsLoggedIn({isLoggedIn: false}))
    this.router.navigate(['/']);
  }

  private handleLoginState() {
    if (!this.isTokenNotExpired) {
      this.store.dispatch(setIsLoggedIn({isLoggedIn: false}))
    }
    setTimeout(() => {
      this.handleLoginState();
    }, 2000);
  }
}
