import { IOpinionDTO } from './opinion-dto.interface';

export interface IAddOpinionDTO extends IOpinionDTO {
    markedCraftsmanId: number;
}
