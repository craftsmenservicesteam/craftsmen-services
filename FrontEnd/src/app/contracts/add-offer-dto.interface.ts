export interface IAddOfferDTO {
    title: string;
    description: string;
    categoryId: number;
    subcategoryId: number;
    creationDate: Date;
    availableLocation: string;
    price: number;
    pricePerHour: number;
    doorToDoor: boolean;
    hasOwnTools: boolean;
    imageBase64: string;
}
