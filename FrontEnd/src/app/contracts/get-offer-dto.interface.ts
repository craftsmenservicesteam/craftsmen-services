import {ICraftsmanProfile} from '../modules/user-profile/interfaces/craftsman-profile.interface';

export interface IGetOfferDTO {
    id: number;
    title: string;
    description: string;
    categoryName: string;
    subcategoryName: string;
    availableLocation: string;
    price: number;
    pricePerHour: number;
    doorToDoor: boolean;
    hasOwnTools: boolean;
    craftsman: ICraftsmanProfile;
}
