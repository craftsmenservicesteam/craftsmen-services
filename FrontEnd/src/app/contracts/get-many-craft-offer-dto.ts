import { IGetOfferDTO } from './get-offer-dto.interface';

export interface IGetManyCraftOffersDTO {
    offers: IGetOfferDTO[];
    count: number;
}
