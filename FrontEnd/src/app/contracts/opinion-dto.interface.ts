export interface IOpinionDTO {
    rate: number;
    description: string;
    creationDate: Date;
}

export function getEmptyOpinion() {
    return {
        rate: 6,
        description: "",
        creationDate: null
    }
}