export interface IFilters {
    offset: number,
    hits: number,
    categoryId: number,
    contains?: string,
    location?: string
}