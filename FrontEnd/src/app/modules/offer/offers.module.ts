import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatDividerModule, MatCheckboxModule, MatExpansionModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { OfferComponent } from './components/offer/offer.component';
import { OffersListComponent } from './pages/offersList/offers-list.component';
import { OfferPreviewComponent } from './pages/offerPreview/offer-preview.component';
import { CraftsmanProfileModule } from '../craftsman-profile/craftsman-profile.module';
import { MatTabsModule, MatButtonModule } from '@angular/material';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FiltersComponent } from './components/filters/filters.component';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        BrowserAnimationsModule,
        MatCardModule,
        SharedModule,
        MatDividerModule,
        MatCheckboxModule,
        CraftsmanProfileModule,
        MatTabsModule,
        MatButtonModule,
        MatExpansionModule,
        MatPaginatorModule,
        MatSelectModule,
        MatInputModule
    ],
    declarations: [
        OffersListComponent,
        OfferComponent,
        OfferPreviewComponent,
        LandingPageComponent,
        FiltersComponent
    ],
    exports: [
        OffersListComponent,
        OfferPreviewComponent,
    ],
    providers: [

    ]
})
export class OffersModule { }
