import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IGetOfferDTO } from '../../../../contracts/get-offer-dto.interface';
import { OfferHttpService } from '../../../../core/http/offer-http.service';

@Component({
    selector: 'app-offer-preview',
    templateUrl: './offer-preview.component.html'
})
export class OfferPreviewComponent implements OnInit {
  public model: IGetOfferDTO;
  constructor(
    private offerHttp: OfferHttpService,
    private route: ActivatedRoute) {
  }
  ngOnInit(): void {
    const offerId = this.route.snapshot.paramMap.get('id');
    this.offerHttp.GetOfferById(Number(offerId)).subscribe(response => {
      return this.model = response.body;
    });
  }
}
