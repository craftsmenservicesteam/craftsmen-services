import { Component, OnInit } from '@angular/core';
import { OfferHttpService } from '../../../../core/http/offer-http.service';
import { IGetOfferDTO } from '../../../../contracts/get-offer-dto.interface';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../../../core/store/state/app.state';
import { selectFilters } from '../../../../core/store/selectors/filters.selector';
import { IFilters } from '../../../../contracts/filters.interface';
import { setFilters } from '../../../../core/store/actions/filters.actions';

@Component({
  selector: 'app-offers-list',
  templateUrl: 'offers-list.component.html',
  styleUrls: ['offers-list.component.scss']
})
export class OffersListComponent implements OnInit {
  public offers: IGetOfferDTO [];
  public filters: IFilters;
  public count: number = 0;
  public filtersExpanded = false;
  public pageSizeOptions: number[] = [5, 10, 15, 20];

  constructor(
    private offerHttp: OfferHttpService,
    private store: Store<IAppState>,
  ) { }

  ngOnInit(): void {
    this.store.pipe(select(selectFilters))
      .subscribe(filters => {
        this.offerHttp.GetManyWithFilters(filters)
          .subscribe(res => {
            this.offers = res.body.offers;
            this.count = res.body.count;
            this.filters = JSON.parse(JSON.stringify(filters));
          })
      })
  }

  public onFiltersSelected() {
    this.filtersExpanded = false;
  }

  public onPageChanged(event: any) {
    this.filters.hits = event.pageSize;
    this.filters.offset = event.pageIndex * event.pageSize;
    this.store.dispatch(setFilters({filters: this.filters}));
  }
}
