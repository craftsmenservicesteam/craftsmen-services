import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { IFilters } from '../../../../contracts/filters.interface';
import { Category } from '../../../addOffer/components/addOfferForm/add-offer-form.component';
import { OfferHttpService } from '../../../../core/http/offer-http.service';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../../core/store/state/app.state';
import { setFilters } from '../../../../core/store/actions/filters.actions';
import { initialFiltersSate } from '../../../../core/store/state/filters.state';

@Component({
    selector: 'filters',
    templateUrl: './filters.component.html',
    styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
    @Input() filters: IFilters;
    @Output() filtersSelected: EventEmitter<boolean> = 
        new EventEmitter();
    public categories: Category[]

    constructor(
        private offerHttpService: OfferHttpService,
        private store: Store<IAppState>
    ) { }

    ngOnInit(): void {
        this.offerHttpService.GetCategories()
            .subscribe(c => this.categories = c);
    }

    public filter() {
        this.store.dispatch(setFilters({filters: this.filters}));
        this.filtersSelected.next(true);
    }

    public reset() {
        this.store.dispatch(setFilters({filters: initialFiltersSate}));
    }
}
