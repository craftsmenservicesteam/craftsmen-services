import { Component, Input } from '@angular/core';
import { IGetOfferDTO } from '../../../../contracts/get-offer-dto.interface';

@Component({
    selector: 'offer',
    templateUrl: './offer.component.html',
    styleUrls: ['./offer.component.scss']
})
export class OfferComponent {
    @Input() model: IGetOfferDTO;
    public disabled = true;
}
