import {NgModule} from "@angular/core";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTooltipModule
} from '@angular/material';
import {AddOfferFormComponent} from './components/addOfferForm/add-offer-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatSnackBarModule,
        MatOptionModule,
        MatSelectModule,
        MatCheckboxModule,
        MatTooltipModule
    ],
    declarations: [
        AddOfferFormComponent
    ],
    exports: [
        AddOfferFormComponent
    ]
})
export class AddOfferModule { }
