import {Component, OnInit} from '@angular/core';
import {OfferHttpService} from '../../../../core/http/offer-http.service';
import {IAddOfferDTO} from '../../../../contracts/add-offer-dto.interface';
import {AddOfferFormHelper} from './add-offer-form.helper';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router'
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

export interface Category {
    id: number;
    name: string;
}

export interface Subcategory {
    id: number;
    name: string;
    categoryId: number;
}

@Component({
    selector: 'add-offer-form',
    templateUrl: 'add-offer-form.component.html',
    styleUrls: ['add-offer-form.component.scss']
})
export class AddOfferFormComponent implements OnInit {

    public model: IAddOfferDTO;
    public categories: Category[];
    public subcategories: Subcategory[];
    public addOfferForm: FormGroup;

    get subcategoriesShown(): Subcategory[] {
        if (!this.model.categoryId) return null;
        return this.subcategories.filter(element => element.categoryId === this.model.categoryId);
    }

    constructor(
        private offerHttp: OfferHttpService,
        private snackBar: MatSnackBar,
        private router: Router,
        private fb: FormBuilder
    ) { }

    ngOnInit(): void {
        this.model = AddOfferFormHelper.getEmptyDTO();

        this.offerHttp.GetCategories().subscribe(cat => {
            this.categories = cat;
        });
        this.offerHttp.GetSubcategories().subscribe((sub => {
            this.subcategories = sub;
        }));

        this.addOfferForm = this.fb.group({
            title: new FormControl(null, Validators.required),
            categorySelector: new FormControl(null, Validators.required),
            subcategorySelector: new FormControl(null, Validators.required),
            description: new FormControl(null, Validators.required),
            doorToDoor: new FormControl(null, Validators.nullValidator),
            hasOwnTools: new FormControl(null, Validators.nullValidator),
            availableLocation: new FormControl(null, Validators.required),
            price: new FormControl(null, Validators.min(0)),
            pricePerHour: new FormControl(null, Validators.min(0))
        })

    }

    public Add() {
        if (AddOfferFormHelper.validate(this.model)) {

            this.model.creationDate = new Date();

            this.offerHttp
                .Add(this.model)
                .subscribe(x => {
                    this.model = AddOfferFormHelper.getEmptyDTO();
                    this.router.navigate(["/"]);
                    this.snackBar.open("Added", "", {
                        duration: 2000,
                      });
                });
        }
    }

    onFileChange(evt){
        const file = evt.target.files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            console.log(reader.result);
            this.model.imageBase64 = reader.result as string;
        };
      }

    public Reset() {
        this.model = AddOfferFormHelper.getEmptyDTO();
    }
}
