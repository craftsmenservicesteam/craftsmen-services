import {IAddOfferDTO} from '../../../../contracts/add-offer-dto.interface';

export class AddOfferFormHelper {
    public static getEmptyDTO(): IAddOfferDTO {
        return {
            title: null,
            description: null,
            categoryId: null,
            subcategoryId: null,
            availableLocation: null,
            creationDate: null,
            price: null,
            pricePerHour: null,
            doorToDoor: false,
            hasOwnTools: false,
            imageBase64: null
        }
    }

    public static validate(offer: IAddOfferDTO): boolean {
        if (offer.title
            && offer.description
            && offer.categoryId
            && offer.subcategoryId
            && offer.price
            && offer.pricePerHour
            && offer.availableLocation != null) {
            return true;
        }
        return false;
    }
}
