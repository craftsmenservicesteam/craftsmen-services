import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { WelcomePageComponent } from './welcome-page.component';

@NgModule({
    imports: [
        MatCardModule,
        MatButtonModule
    ],
    declarations: [
        WelcomePageComponent
    ],
    exports: [
        WelcomePageComponent
    ]
})
export class WelcomePageModule { }
