export interface IUserProfile {
    id: number;
    name: string;
    surname: string;
    email: string;
    isCraftsman: boolean;
}
