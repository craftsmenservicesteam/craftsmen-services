import { IUserProfile } from './user-profile.interface';
import { IWeekDays } from '../../../shared/interfaces/week-days.interface';

export interface ICraftsmanProfile extends IUserProfile, IWeekDays {
    craftsmanId?: number;
    company: string;
    description: string;
    phoneNumber: string;
    latitude: number;
    longitude: number;
    rate: number;
    business: string;
}

export function getEmptyCraftsmanProfile() {
    return {
        id: 0,
        name: "",
        surname: "",
        email: "",
        isCraftsman: false,
        company: "",
        description: "",
        phoneNumber: "",
        latitude: 0,
        longitude: 0,
        business: "",
        rate: null,
        monday: {
            startTime: "00:00",
            endTime: "00:00"
        },
        tuesday: {
            startTime: "00:00",
            endTime: "00:00"
        },
        wednesday: {
            startTime: "00:00",
            endTime: "00:00"
        },
        thursday: {
            startTime: "00:00",
            endTime: "00:00"
        },
        friday: {
            startTime: "00:00",
            endTime: "00:00"
        },
        saturday: {
            startTime: "00:00",
            endTime: "00:00"
        },
        sunday: {
            startTime: "00:00",
            endTime: "00:00"
        }
    }
}
