import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileOffersComponent } from './user-profile-offers.component';

describe('UserProfileOffersComponent', () => {
  let component: UserProfileOffersComponent;
  let fixture: ComponentFixture<UserProfileOffersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileOffersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
