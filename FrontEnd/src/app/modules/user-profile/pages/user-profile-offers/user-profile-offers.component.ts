import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IGetOfferDTO } from 'src/app/contracts/get-offer-dto.interface';
import { OfferHttpService } from 'src/app/core/http/offer-http.service';

@Component({
  selector: 'app-user-profile-offers',
  templateUrl: './user-profile-offers.component.html',
  styleUrls: ['./user-profile-offers.component.scss']
})
export class UserProfileOffersComponent implements OnInit {
  constructor(
    private offerHttp: OfferHttpService,
    private route: ActivatedRoute
  ) { }
  public offers: IGetOfferDTO [];

  @Input() craftsmanId: number;

  ngOnInit() {
    this.offerHttp.GetOffersByCraftsmanId(this.craftsmanId)
      .subscribe(x => {
        this.offers = x.body.offers;
      });
  }

}
