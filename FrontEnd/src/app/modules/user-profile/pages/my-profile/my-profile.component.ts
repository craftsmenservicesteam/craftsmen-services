import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IMyProfileState } from '../../../../core/store/state/my-profile.state';
import { Store, select } from '@ngrx/store';
import { selectMyProfile } from '../../../../core/store/selectors/my-profile.selectors';
import { IAppState } from '../../../../core/store/state/app.state';
import { MatDialog } from '@angular/material';
import { EditProfileHelper } from '../../service/edit-profile.helper';


@Component({
  selector: 'my-profile',
  templateUrl: 'my-profile.component.html',
  styleUrls: ['my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {
  public myProfile$: Observable<IMyProfileState>;
  constructor(
    private store: Store<IAppState>,
    private editProfileHelper: EditProfileHelper,
    public dialog: MatDialog
  ) {
    this.myProfile$ = this.store.pipe(select(selectMyProfile));
  }

  public updateMyProfile() {
    this.myProfile$.subscribe(profile => {
      setTimeout(() => this.editProfileHelper.handleUpdateProfile(profile), 0);
    });
  }

  ngOnInit(): void {
    this.myProfile$.subscribe(profile => {
      if (!profile) {
        setTimeout(() => this.editProfileHelper.handleNoProfile(), 0);
      }
    });
  }
}
