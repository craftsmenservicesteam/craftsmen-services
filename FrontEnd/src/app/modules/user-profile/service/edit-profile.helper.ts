import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material';
import { IAppState } from '../../../core/store/state/app.state';
import { EditMyProfileModalComponent } from '../modals/editMyProfile/edit-my-profile.component';
import { UserProfileHttpService } from '../../../core/http/user-profile-http.service';
import { Router } from '@angular/router';
import { getMyProfile } from '../../../core/store/actions/my-profile.actions';
import { ICraftsmanProfile } from '../interfaces/craftsman-profile.interface';

@Injectable()
export class EditProfileHelper {
    constructor(
        private store: Store<IAppState>,
        private http: UserProfileHttpService,
        private router: Router,
        public dialog: MatDialog
    ) { }

    public handleNoProfile() {
        const dialogRef = this.dialog.open(EditMyProfileModalComponent, {
            width: '700px',
            maxHeight: '700px',
            data: null,
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.http.AddMyProfile(result)
                    .subscribe(response => {
                        if (response.succeeded) {
                            this.store.dispatch(getMyProfile());
                        } else {
                            this.router.navigate(['/']);
                        }
                    });
            } else {
                this.router.navigate(['/']);
            }
        });
    }

    public handleUpdateProfile(profile: ICraftsmanProfile) {
        const dialogRef = this.dialog.open(EditMyProfileModalComponent, {
            width: '700px',
            maxHeight: '700px',
            data: {
                isUpdating: true,
                isCraftsman: profile.isCraftsman,
                id: profile.id,
                monday: {
                    startTime: profile.monday.startTime,
                    endTime: profile.monday.endTime
                },
                tuesday: {
                    startTime: profile.tuesday.startTime,
                    endTime: profile.tuesday.endTime,
                },
                wednesday: {
                    startTime: profile.wednesday.startTime,
                    endTime: profile.wednesday.endTime
                },
                thursday: {
                    startTime: profile.thursday.startTime,
                    endTime: profile.thursday.endTime
                },
                friday: {
                    startTime: profile.friday.startTime,
                    endTime: profile.friday.endTime
                },
                saturday: {
                    startTime: profile.saturday.startTime,
                    endTime: profile.saturday.endTime
                },
                sunday: {
                    startTime: profile.sunday.startTime,
                    endTime: profile.sunday.endTime
                }
            },
            disableClose: false
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.http.UpdateMyProfile(result)
                    .subscribe(response => {
                        if (response.succeeded) {
                            this.store.dispatch(getMyProfile());
                        }
                    });
            }
        });
    }
}
