import { NgModule } from '@angular/core';
import { UserBaseSettingsComponent } from './components/user-base-settings/user-base-settings.component';
import { CraftsmanSettingsComponent } from './components/craftsman-settings/craftsman-settings.component';
import { UserSettingsFormComponent } from './components/user-settings-form/user-settings-form.component';
import {
    MatSnackBarModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatSliderModule,
    MatTabsModule,
    MatCheckboxModule,
    MatSnackBar,
    MatDialogModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../../shared/shared.module';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';
import { EditMyProfileModalComponent } from './modals/editMyProfile/edit-my-profile.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { EditProfileHelper } from './service/edit-profile.helper';
import { UserProfileOffersComponent } from './pages/user-profile-offers/user-profile-offers.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatSnackBarModule,
        MatSliderModule,
        MatCheckboxModule,
        SharedModule,
        MatDialogModule,
        MatGridListModule,
        MatTabsModule
    ],
    declarations: [
        UserBaseSettingsComponent,
        CraftsmanSettingsComponent,
        UserSettingsFormComponent,
        MyProfileComponent,
        EditMyProfileModalComponent,
        UserProfileOffersComponent
    ],
    exports: [
        MyProfileComponent
    ],
    providers: [
        MatSnackBar,
        EditProfileHelper
    ],
    entryComponents: [
        EditMyProfileModalComponent
    ]
})
export class UserProfileModule { }
