import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ICraftsmanProfile} from '../../interfaces/craftsman-profile.interface';

@Component({
    selector: 'user-settings-form',
    templateUrl: 'user-settings-form.component.html',
    styleUrls: ['user-settings-form.component.scss']
})
export class UserSettingsFormComponent {
    @Input() userProfile: ICraftsmanProfile;
    @Output() isValid: EventEmitter<boolean> = new EventEmitter();

    parentFormValidation = {
        userBase: false,
        craftsmanSettings: false
    };

    setUserBaseValidationValue(v) {
        this.parentFormValidation.userBase = v;

        this.isValid.emit(this.parentFormValidation.userBase);
    }

    setCraftsmanValidationValue(v) {
        this.parentFormValidation.craftsmanSettings = v;

        this.isValid.emit(this.parentFormValidation.userBase && this.parentFormValidation.craftsmanSettings);
    }
}
