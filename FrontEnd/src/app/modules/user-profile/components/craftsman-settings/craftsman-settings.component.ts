import { ICraftsmanProfile } from '../../interfaces/craftsman-profile.interface';
import { DAYS_DICTIONARY } from '../../../../shared/consts/days.dictionary';
import { ITimeRangeView } from '../../../../shared/interfaces/time-range-view.interface';
import { TimeHelper } from '../../../../shared/helpers/time.helper';
import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'craftsman-settings',
    templateUrl: 'craftsman-settings.component.html',
    styleUrls: ['craftsman-settings.component.scss']
})
export class CraftsmanSettingsComponent implements OnInit {
    @Input() userProfile: ICraftsmanProfile;
    @Output() isValid: EventEmitter<boolean> = new EventEmitter();
    public timeRangeViews: ITimeRangeView[] = [];
    public craftsmanSettingsForm: FormGroup;

    constructor(private fb: FormBuilder) {
        this.craftsmanSettingsForm = this.fb.group({
            company: new FormControl(null, Validators.required),
            description: new FormControl(null, Validators.required),
            phoneNumber: new FormControl(null, Validators.minLength(9))
        });

        this.craftsmanSettingsForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.craftsmanSettingsForm.valid);
        });
    }

    setLocation(event) {
        this.userProfile.latitude = event.latitude;
        this.userProfile.longitude = event.longitude;
    }

    getNewTimeRanges() {
        this.timeRangeViews = TimeHelper
            .getTimeRangeViews(this.userProfile);
    }

    ngOnInit(): void {
        console.log(this.userProfile);
        this.timeRangeViews = TimeHelper
            .getTimeRangeViews(this.userProfile);
    }
}
