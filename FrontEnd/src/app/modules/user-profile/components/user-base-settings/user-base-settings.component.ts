import {Component, Input, Output, EventEmitter} from '@angular/core';
import {IUserProfile} from '../../interfaces/user-profile.interface';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'user-base-settings',
    templateUrl: 'user-base-settings.component.html',
    styleUrls: ['user-base-settings.component.scss']
})
export class UserBaseSettingsComponent {
    @Input() userProfile: IUserProfile;
    @Output() isValid: EventEmitter<boolean> = new EventEmitter();
    public userSettingsForm: FormGroup;

    constructor(private fb: FormBuilder) {
        this.userSettingsForm = this.fb.group({
            name: new FormControl(null, Validators.required),
            surname: new FormControl(null, Validators.required),
            email: new FormControl(null, Validators.email),
            isCraftsmanBool: new FormControl(null, null)
        });

        this.userSettingsForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.userSettingsForm.valid);
        });
    }
}
