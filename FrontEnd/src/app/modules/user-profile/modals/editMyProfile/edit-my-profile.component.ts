import { Component, Inject, OnInit, Input } from '@angular/core';
import { getEmptyCraftsmanProfile, ICraftsmanProfile } from '../../interfaces/craftsman-profile.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'edit-my-profile',
    templateUrl: 'edit-my-profile.component.html',
    styleUrls: ['edit-my-profile.component.scss']
})
export class EditMyProfileModalComponent implements OnInit {
    valid: boolean;

    constructor(
        public dialogRef: MatDialogRef<EditMyProfileModalComponent>,
        @Inject(MAT_DIALOG_DATA) public userProfile: ICraftsmanProfile
    ) { }

    ngOnInit(): void {
        if (!this.userProfile) {
            this.userProfile = getEmptyCraftsmanProfile();
        }
    }

    public close() {
        this.dialogRef.close()
    }
}
