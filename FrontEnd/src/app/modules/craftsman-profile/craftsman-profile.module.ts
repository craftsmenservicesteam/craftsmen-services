import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { CraftsmanProfileHeaderComponent } from './components/craftsman-profile-header/craftsman-profile-header.component';
import { CraftsmanProfilePageComponent } from './pages/craftsman-profile-page/craftsman-profile-page.component';
import { CraftsmanProfileBodyComponent } from './components/craftsman-profile-body/craftsman-profile-body.component';
import { MatTabsModule, MatInputModule } from '@angular/material';
import { CraftsmanProfileAboutComponent } from './components/craftsman-profile-about/craftsman-profile-about.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CraftsmanProfileOffersComponent } from './components/craftsman-profile-offers/craftsman-profile-offers.component';
import { SharedModule } from '../../shared/shared.module';
import { OpinionsListComponent } from './components/opinions/opinion-list.component';
import { MatTableModule } from '@angular/material/table'
import { AddOpinionComponent } from './components/add-opinion/add-opinion.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CraftsmanProfileHeaderComponent,
    CraftsmanProfileBodyComponent,
    CraftsmanProfileAboutComponent,
    CraftsmanProfileOffersComponent,
    CraftsmanProfilePageComponent,
    OpinionsListComponent,
    AddOpinionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB2q1wC1JZjR3bR6LvhyQZ2wX_dxdplhY0'
    }),
    MatTabsModule,
    MatExpansionModule,
    MatCardModule,
    MatButtonModule,
    SharedModule,
    MatTableModule,
    MatInputModule
  ],
  exports: [
    CraftsmanProfilePageComponent
  ]
})
export class CraftsmanProfileModule { }
