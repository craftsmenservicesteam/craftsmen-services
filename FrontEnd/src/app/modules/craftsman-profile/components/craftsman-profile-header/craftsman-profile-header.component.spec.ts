import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraftsmanProfileHeaderComponent } from './craftsman-profile-header.component';

describe('CraftsmanProfileHeaderComponent', () => {
  let component: CraftsmanProfileHeaderComponent;
  let fixture: ComponentFixture<CraftsmanProfileHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraftsmanProfileHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraftsmanProfileHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
