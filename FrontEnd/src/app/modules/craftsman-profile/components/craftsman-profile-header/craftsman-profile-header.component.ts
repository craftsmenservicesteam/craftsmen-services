import { Component, OnInit, Input } from '@angular/core';
import { ICraftsmanProfile } from '../../../user-profile/interfaces/craftsman-profile.interface';

@Component({
  selector: 'app-craftsman-profile-header',
  templateUrl: './craftsman-profile-header.component.html',
  styleUrls: ['./craftsman-profile-header.component.scss']
})
export class CraftsmanProfileHeaderComponent {
  @Input() craftsman: ICraftsmanProfile;
}
