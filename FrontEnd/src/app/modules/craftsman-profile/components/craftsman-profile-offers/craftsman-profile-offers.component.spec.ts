import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraftsmanProfileOffersComponent } from './craftsman-profile-offers.component';

describe('CraftsmanProfileOffersComponent', () => {
  let component: CraftsmanProfileOffersComponent;
  let fixture: ComponentFixture<CraftsmanProfileOffersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraftsmanProfileOffersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraftsmanProfileOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
