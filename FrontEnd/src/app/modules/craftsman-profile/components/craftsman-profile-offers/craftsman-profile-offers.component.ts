import { Component, Input, OnInit } from '@angular/core';
import { OfferHttpService } from '../../../../core/http/offer-http.service';
import { IGetOfferDTO } from '../../../../contracts/get-offer-dto.interface';

@Component({
  selector: 'app-craftsman-profile-offers',
  templateUrl: './craftsman-profile-offers.component.html',
  styleUrls: ['./craftsman-profile-offers.component.scss']
})
export class CraftsmanProfileOffersComponent implements OnInit {
  @Input() craftsmanId: number;
  public offers: IGetOfferDTO[];

  constructor(
    public offerHttpService: OfferHttpService
  ) { }

  ngOnInit(): void {
    this.offerHttpService.GetOffersByCraftsmanId(this.craftsmanId)
      .subscribe(x => this.offers = x.body.offers);
  }
}
