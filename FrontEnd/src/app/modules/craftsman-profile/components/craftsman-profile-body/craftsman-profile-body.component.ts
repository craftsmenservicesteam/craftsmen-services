import { Component, OnInit, Input } from '@angular/core';
import { ICraftsmanProfile } from '../../../user-profile/interfaces/craftsman-profile.interface';

@Component({
  selector: 'app-craftsman-profile-body',
  templateUrl: './craftsman-profile-body.component.html',
  styleUrls: ['./craftsman-profile-body.component.scss']
})
export class CraftsmanProfileBodyComponent {
  @Input() craftsman: ICraftsmanProfile;
}
