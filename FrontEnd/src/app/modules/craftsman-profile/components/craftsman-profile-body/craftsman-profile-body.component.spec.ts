import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraftsmanProfileBodyComponent } from './craftsman-profile-body.component';

describe('UserProfileBodyComponent', () => {
  let component: CraftsmanProfileBodyComponent;
  let fixture: ComponentFixture<CraftsmanProfileBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraftsmanProfileBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraftsmanProfileBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
