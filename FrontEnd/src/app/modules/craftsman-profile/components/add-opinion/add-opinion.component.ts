import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IOpinionDTO, getEmptyOpinion } from '../../../../contracts/opinion-dto.interface';
import { OpinionHttpService } from '../../../../core/http/opinion.http.service';
import { Router } from '@angular/router';

@Component({
    selector: 'add-opinion',
    templateUrl: 'add-opinion.component.html',
    styleUrls: ['add-opinion.component.scss']
})
export class AddOpinionComponent {
    @Input() craftsmanId: number;
    @Output() opinionAdded: EventEmitter<boolean> = new EventEmitter();
    
    public opinion: IOpinionDTO = getEmptyOpinion();
    
    constructor(
        public opinionHttpService: OpinionHttpService,
        private router: Router
    ) { }

    public add() {
        this.opinionHttpService.Add({
            ...this.opinion,
            markedCraftsmanId: this.craftsmanId,
            creationDate: new Date()
        }).subscribe(x => {
            this.opinionAdded.next(true);
        });
    }

    public onValueChanged(
        event: number
    ) {
        this.opinion.rate = event;
    }
}
