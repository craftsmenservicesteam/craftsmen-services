import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraftsmanProfileAboutComponent } from './craftsman-profile-about.component';

describe('CraftsmanProfileAboutComponent', () => {
  let component: CraftsmanProfileAboutComponent;
  let fixture: ComponentFixture<CraftsmanProfileAboutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraftsmanProfileAboutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraftsmanProfileAboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
