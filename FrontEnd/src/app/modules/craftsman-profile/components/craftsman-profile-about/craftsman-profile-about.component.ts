import { Component, OnInit, Input } from '@angular/core';
import { ICraftsmanProfile } from '../../../user-profile/interfaces/craftsman-profile.interface';
import { TimeHelper } from '../../../../shared/helpers/time.helper';

@Component({
  selector: 'app-craftsman-profile-about',
  templateUrl: './craftsman-profile-about.component.html',
  styleUrls: ['./craftsman-profile-about.component.scss']
})
export class CraftsmanProfileAboutComponent implements OnInit {
  @Input() craftsman: ICraftsmanProfile;

  public workingDays: string[];

  constructor() { }

  ngOnInit() {
    this.workingDays = TimeHelper.getTimeRangeViews(this.craftsman)
      .map(x => {
        return (x.title && x.timeRange)
          ? `${x.title}: ${x.timeRange.startTime} - ${x.timeRange.endTime}`
          : '';
      })
  }
}
