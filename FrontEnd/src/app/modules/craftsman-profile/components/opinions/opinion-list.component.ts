import { Component, Input, OnInit } from '@angular/core';
import { IOpinionDTO } from '../../../../contracts/opinion-dto.interface';
import { OpinionHttpService } from '../../../../core/http/opinion.http.service';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../../../core/store/state/app.state';
import { Observable } from '../../../../../../node_modules/rxjs';
import { ICraftsmanProfile } from '../../../user-profile/interfaces/craftsman-profile.interface';
import { selectMyProfile } from '../../../../core/store/selectors/my-profile.selectors';
import { selectIsLoggedIn } from '../../../../core/store/selectors/login.selectors';

@Component({
    selector: 'opinions-list',
    templateUrl: 'opinion-list.component.html',
    styleUrls: ['opinion-list.component.scss']
})
export class OpinionsListComponent implements OnInit {
    @Input() craftsmanId: number;
    public opinions: IOpinionDTO[];
    public myProfile$: Observable<ICraftsmanProfile>;
    public addOfferExpanded = false;
    public isLoggedIn$: Observable<boolean>;

    public displayedColumns = [
        'creationDate',
        'rate',
        'description'
    ]

    constructor(
        private store: Store<IAppState>,
        public opinionHttpService: OpinionHttpService
    ) { }

    ngOnInit(): void {
        this.opinionHttpService.GetByCraftsmanId(this.craftsmanId)
            .subscribe(x => this.opinions = x.body);
        this.myProfile$ = this.store.pipe(select(selectMyProfile));
        this.isLoggedIn$ = this.store.pipe(select(selectIsLoggedIn));
    }

    public refresh() {
        this.opinionHttpService.GetByCraftsmanId(this.craftsmanId)
            .subscribe(x => this.opinions = x.body);

        this.addOfferExpanded = false;
    }
}
