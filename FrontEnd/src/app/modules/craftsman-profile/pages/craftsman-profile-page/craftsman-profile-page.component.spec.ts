import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraftsmanProfilePageComponent } from './craftsman-profile-page.component';

describe('CraftsmanProfilePageComponent', () => {
  let component: CraftsmanProfilePageComponent;
  let fixture: ComponentFixture<CraftsmanProfilePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraftsmanProfilePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraftsmanProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
