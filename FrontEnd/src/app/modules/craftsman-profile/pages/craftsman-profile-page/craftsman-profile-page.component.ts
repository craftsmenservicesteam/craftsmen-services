import { Component, Input } from '@angular/core';
import { ICraftsmanProfile } from '../../../user-profile/interfaces/craftsman-profile.interface';

@Component({
  selector: 'craftsman-profile-page',
  templateUrl: './craftsman-profile-page.component.html',
  styleUrls: ['./craftsman-profile-page.component.scss']
})
export class CraftsmanProfilePageComponent {
  @Input() craftsmanProfile: ICraftsmanProfile;
}
