import { CraftsmanOffersList } from '../interfaces/craftsman-offer';

export const mockedAllOffers: CraftsmanOffersList = {
  offers: [
    {
      title: 'Składanie mebli z IKEI',
      description: 'Kupiłeś me',
      price: 200,
      pricePerHour: 2000,
      images: [
        {
          src: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
          alt: 'Photo of a Shiba Inu'
        }
      ],
    },
    {
      title: 'Składanie mebli z IKEI',
      description: 'Kupiłeś me',
      price: 200,
      pricePerHour: 2000,
      images: [
        {
          src: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
          alt: 'Photo of a Shiba Inu'
        }
      ],
    },
    {
      title: 'Składanie mebli z IKEI',
      description: 'Kupiłeś me',
      price: 200,
      pricePerHour: 2000,
      images: [
        {
          src: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
          alt: 'Photo of a Shiba Inu'
        }
      ],
    },
    {
      title: 'Składanie mebli z IKEI',
      description: 'Kupiłeś me',
      price: 200,
      pricePerHour: 2000,
      images: [
        {
          src: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
          alt: 'Photo of a Shiba Inu'
        }
      ],
    },
    {
      title: 'Składanie mebli z IKEI',
      description: 'Kupiłeś me',
      price: 200,
      pricePerHour: 2000,
      images: [
        {
          src: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
          alt: 'Photo of a Shiba Inu'
        }
      ],
    },
    {
      title: 'Składanie mebli z IKEI',
      description: 'Kupiłeś me',
      price: 200,
      pricePerHour: 2000,
      images: [
        {
          src: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
          alt: 'Photo of a Shiba Inu'
        }
      ],
    },
  ]
};
