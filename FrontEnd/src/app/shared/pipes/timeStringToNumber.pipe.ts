import { Pipe, PipeTransform } from '@angular/core';
import { TimeHelper } from '../helpers/time.helper';

@Pipe({name: 'timeStringToNumber'})
export class TimeStringToNumberPipe implements PipeTransform {
    transform(time: string) {
        return TimeHelper.timeStringToNumber(time);
    }
}