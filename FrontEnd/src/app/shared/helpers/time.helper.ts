import { ICraftsmanProfile } from '../../modules/user-profile/interfaces/craftsman-profile.interface';
import { DAYS_DICTIONARY } from '../consts/days.dictionary';
import { ITimeRangeView } from '../interfaces/time-range-view.interface';

export class TimeHelper {
    public static numberToTimeString(
        time: number
    ): string {
        const integerString = Math.floor(time) > 9
            ? Math.floor(time).toString()
            : '0' + Math.floor(time).toString();

        const remainderNumber = time % 1;
        const remainderString = remainderNumber === 0
            ? '00'
            : (remainderNumber * 60).toString();

        return `${integerString}:${remainderString}`;
    }

    public static timeStringToNumber(
        time: string
    ) {
        const integer = Number.parseInt(time.substr(0, 2))
        const remainder = Number.parseInt(time.substring(3,5)) / 60;

        return integer + remainder;
    }

    public static getTimeRangeViews(
        craftsmanProfile: ICraftsmanProfile
    ): ITimeRangeView[] {
        var result: ITimeRangeView[] = [];
        for (let prop in craftsmanProfile) {
            if (prop in DAYS_DICTIONARY) {
                result.push({
                    title: DAYS_DICTIONARY[prop].charAt(0).toUpperCase() + DAYS_DICTIONARY[prop].slice(1),
                    timeRange: craftsmanProfile[prop]
                });
            }
        }

        return result;
    }
}