export const DAYS_DICTIONARY: { [id: string] : string } = {
    "monday": "poniedziałek",
    "tuesday": "wtorek",
    "wednesday": "środa",
    "thursday": "czwartek",
    "friday": "piątek",
    "saturday": "sobota",
    "sunday": "niedziela"
};