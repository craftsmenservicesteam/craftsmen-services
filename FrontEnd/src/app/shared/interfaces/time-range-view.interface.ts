import { ITimeRange } from './time-range.interface';

export interface ITimeRangeView {
    timeRange: ITimeRange;
    title: string;
}