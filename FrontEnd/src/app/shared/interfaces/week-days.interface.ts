import { ITimeRange } from './time-range.interface';

export class IWeekDays {
    monday: ITimeRange;
    tuesday: ITimeRange;
    wednesday: ITimeRange;
    thursday: ITimeRange;
    friday: ITimeRange;
    saturday: ITimeRange;
    sunday: ITimeRange;
}