export interface ITimeRange {
    startTime: string;
    endTime: string;
}