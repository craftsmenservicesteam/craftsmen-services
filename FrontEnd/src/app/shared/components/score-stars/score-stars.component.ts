import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'score-stars',
    template: `
        <span *ngFor="let star of stars">
            <div *ngIf="star" class="material-icons contactIcon">star</div>
            <div *ngIf="!star" class="material-icons contactIcon">star_border</div>
        </span> 
    `
})
export class ScoreStarsComponent implements OnInit {
    @Input() value: number;
    public stars: boolean[] = [];

    ngOnInit(): void {
        this.value = this.value || 0;

        for (let i = 0; i < this.value; i++) {
            this.stars.push(true);
        }

        for (let i = this.value; i < 6; i++) {
            this.stars.push(false);
        }
    }
}