import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'score-stars-picker',
    template: `
        <span *ngFor="let star of stars; let i = index">
            <div *ngIf="star" (click)="onValueChanged(i)" class="material-icons contactIcon">star</div>
            <div *ngIf="!star" (click)="onValueChanged(i)" class="material-icons contactIcon">star_border</div>
        </span> 
    `
})
export class ScoreStarsPickerComponent implements OnInit {
    @Output() value: EventEmitter<number> = new EventEmitter();
    public stars: boolean[] = [];

    ngOnInit(): void {
        for (let i = 0; i < 6; i++) {
            this.stars.push(true);
        }
    }

    public onValueChanged(
        value: number
    ) {
        this.stars = [];
        for (let i = 0; i < value + 1; i++) {
            this.stars.push(true);
        }

        for (let i = value + 1; i < 6; i++) {
            this.stars.push(false);
        }
        this.value.next(value + 1);
    }
}