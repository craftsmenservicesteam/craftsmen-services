import { Component, Input, OnInit } from '@angular/core';
import { TimeHelper } from '../../helpers/time.helper';
import { ITimeRangeView } from '../../interfaces/time-range-view.interface';

@Component({
    selector: 'time-range-picker',
    templateUrl: 'time-range-picker.component.html',
    styleUrls: ['time-range-picker.component.scss']
})
export class TimeRangePickerComponent implements OnInit {
    @Input() model: ITimeRangeView
    public timeHelper = TimeHelper;
    public step = 0.25;
    public startTimeInt: number;
    public endTimeInt: number;

    ngOnInit(): void {
        this.startTimeInt = this.model.timeRange.startTime
            ? TimeHelper.timeStringToNumber(this.model.timeRange.startTime)
            : 0;

        this.endTimeInt = this.model.timeRange.endTime
            ? TimeHelper.timeStringToNumber(this.model.timeRange.endTime)
            : 0;
    }

    public applyStartTime(
        time: number
    ){
        if (time > this.endTimeInt) {
            this.endTimeInt = time;
            this.model.timeRange.endTime = TimeHelper.numberToTimeString(time);
        }
        this.model.timeRange.startTime = TimeHelper.numberToTimeString(time);        
    }

    public applyEndTime(
        time: number
    ){
        if (time < this.startTimeInt) {
            this.startTimeInt = time;
            this.model.timeRange.startTime = TimeHelper.numberToTimeString(time);
        }
        this.model.timeRange.endTime = TimeHelper.numberToTimeString(time);   
    }
}