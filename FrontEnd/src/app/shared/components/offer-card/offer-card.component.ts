import { Component, OnInit, Input } from '@angular/core';
import { IGetOfferDTO } from '../../../contracts/get-offer-dto.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-offer-card',
  templateUrl: './offer-card.component.html',
  styleUrls: ['./offer-card.component.scss']
})
export class OfferCardComponent implements OnInit {

  constructor(private router: Router) { }
  @Input() offer: IGetOfferDTO;

  public moveToDetailsPage() {
    this.router.navigate([`/offer/${this.offer.id}`])
  }

  ngOnInit() {
  }
}
