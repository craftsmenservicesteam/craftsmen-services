import { Component, EventEmitter, Output, OnChanges } from '@angular/core';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss']
})
export class LocationPickerComponent {

  constructor() { }
  title = 'Znajdziesz mnie tutaj';
  latitude = 54.3716395;
  longitude = 18.6123305;
  locationChosen = false;
  zoom = 18;

  @Output() changeLocation = new EventEmitter();

  onLocationChanged(event) {
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.locationChosen = true;
    this.changeLocation.emit({ latitude: this.latitude, longitude: this.longitude });
  }

}
