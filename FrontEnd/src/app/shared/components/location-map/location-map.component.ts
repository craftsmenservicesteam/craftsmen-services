import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-location-map',
  templateUrl: './location-map.component.html',
  styleUrls: ['./location-map.component.scss']
})
export class LocationMapComponent implements OnInit {
  zoom = 18;

  @Input() title: string;
  @Input() latitude: number;
  @Input() longitude: number;

  constructor() { }

  ngOnInit() {
  }

}
