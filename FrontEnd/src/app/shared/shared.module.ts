import { NgModule } from '@angular/core';
import { MatSliderModule, MatCardModule, MatButtonModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TimeRangePickerComponent } from './components/time-range-picker/time-range-picker.component';
import { TimeStringToNumberPipe } from './pipes/timeStringToNumber.pipe';
import { OfferCardComponent } from './components/offer-card/offer-card.component';
import { AuthService } from '../core/authentication/auth/auth.service';
import { LocationPickerComponent } from './components/location-picker/location-picker.component';
import { AgmCoreModule } from '@agm/core';
import { LocationMapComponent } from './components/location-map/location-map.component';
import { ScoreStarsComponent } from './components/score-stars/score-stars.component';
import { ScoreStarsPickerComponent } from './components/score-stars-picker/score-stars-picker.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatSliderModule,
        MatCardModule,
        MatButtonModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB2q1wC1JZjR3bR6LvhyQZ2wX_dxdplhY0'
          }),
    ],
    declarations: [
        TimeRangePickerComponent,
        OfferCardComponent,
        LocationPickerComponent,
        LocationMapComponent,
        ScoreStarsComponent,
        ScoreStarsPickerComponent
    ],
    exports: [
        TimeRangePickerComponent,
        OfferCardComponent,
        LocationPickerComponent,
        LocationMapComponent,
        ScoreStarsComponent,
        ScoreStarsPickerComponent
    ],
    providers: [
        TimeStringToNumberPipe,
        AuthService,
    ]
})
export class SharedModule { }
