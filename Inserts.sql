USE [Craftsmen]
GO

INSERT INTO dbo.Category(Name)
VALUES
	('House building'),
	('Renovation'),
	('Design'),
	('Furniture'),
	('Garden'),
	('Painter'),
	('Plumber'),
	('Electrican'),
	('Installation & Repair'),
	('Handyman'),
	('Cleaning'),
	('Administration Services');

INSERT INTO dbo.Subcategory(Name)
VALUES
	('Architect'),
	('Interior desing'),
	('Custom-made furnishing'),
	('Garnden arrangement');

