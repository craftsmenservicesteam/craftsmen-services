# Required tools
1. Node.js
2. Visual Studio (2017) with .Net Core (https://dotnet.microsoft.com/download/visual-studio-sdks)
3. My SQL Server (optional)
4. Visual studio code - VERY RECOMENDED FOR FRONTEND DEVELOPMENT

# Project setup

## Docker

## Requirements

1. Installed Docker
2. Installed docker-compose
3. Terminal (Linux), PowerShell (Windows 10 Pro), docker toolbox (Other Windows)

### Useful commands

source: https://vsupalov.com/cleaning-up-after-docker/
1. Use command `docker-compose up` to build and then run project. Project will be run automatically
2. Use command `docker-compose down -v --rmi all --remove-orphans` to clean everything catched with project or just `docker-compose down` to switch off app and cache
3. `docker rm -v $(docker ps -aq -f 'status=exited')` Remove all exited containers
4. `docker rmi $(docker images -aq -f 'dangling=true')` Remove all docker images
5. `docker volume rm $(docker volume ls -q -f 'dangling=true')` Remove all volumes ATTENTION: this will also remove volumes of docker-compose if the containers are barely stopped

### Important files to recognise between enviroments

#### Backend
1. `src/Craftsmen.WebApi/Properties/launchSettings.json`
```json
{
  "iisSettings": {
    "windowsAuthentication": false,
    "anonymousAuthentication": true,
    "iisExpress": {
    // Here should be https://localhost:5000 in case run project locally
      "applicationUrl": "https://craftsmen.webapi:5000",
      "sslPort": 0
    }
  },
  "$schema": "http://json.schemastore.org/launchsettings.json",
  "profiles": {
    "Craftsmen": {
      "commandName": "Project",
      "launchBrowser": false,
      "launchUrl": "selftest",
      "environmentVariables": {
        "ASPNETCORE_ENVIRONMENT": "Development",
    // Here should be https://localhost:5000 in case run project locally
        "ASPNETCORE_URLS": "https://craftsmen.webapi:5000",
    // On docker version connectionString to database is @"Server=mssql;Database=Craftsmen;User=sa;Password=Your_password123;"
    // but works without that so we can remove it or usefor different versions
        "CRAFTSMEN__ConnectionString": "Server=localhost;Database=Craftsmen;Trusted_Connection=True"
      },
    // Here should be https://localhost:5000 in case run project locally
      "applicationUrl": "https://craftsmen.webapi:5000"
    }
  }
}
```

2. `src/Craftsmen.WebApi/Startup.cs`
Just change commented connectionString with uncommented to database in `ConfigureServices(IServiceCollection services)` method

#### Frontend

1. `proxy.conf.json`
```javascript
{
    "/api/*": {
      "target": "https://craftsmen.webapi:5000",    // Local version should have url https://localhost:5000
      "secure": false,
      "logLevel": "debug",
      "changeOrigin": true,
      "pathRewrite": {"api" : "Public"}
    }
  }
```

## Locally

1. Go to the direstory where you want to clone the project
2. Clone the whole repository
    - `git clone https://siggp@bitbucket.org/craftsmenservicesteam/craftsmen-services.git`
3. Open `Craftsmen.sln`
4. Restore nuget packages:

![](./docs/nugets.png)

5. Set WebApi as startup project

![](./docs/startup.png)

6. Migrate database:
    - In package manage console choose database project and type: `Add-Migration InitialCreate -Context CraftsmenDbContext`

![](./docs/migration.png)

7. Init database
    - In package manage console choose database project and type: `Update-Database -Context CraftsmenDbContext`
    - Check if database was creaded

![](./docs/initdb.png)

![](./docs/database.png)

8. Run backend project (console should be opend)

![](./docs/backend.png)

9. In any console (powershell) go to `\craftsmen-services\FrontEnd`

10. Install all npm packages
    - `npm install`

11. Serve the app 
    - `ng serve`

12. On `http://localhost:4200` a frontend single-page-app should work. Frontend should be integrated with working api (backend console app must be running)
    - At current state there is basic frontend form where u can add some offers and see them. Offers are saved in created in p.7 database!
